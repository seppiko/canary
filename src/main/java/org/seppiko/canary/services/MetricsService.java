/*
 * Copyright 2023 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.seppiko.canary.services;

import org.seppiko.canary.models.metrics.MXBean;
import org.seppiko.canary.models.metrics.Metric;
import org.seppiko.canary.utils.JmxMetricsUtil;
import org.seppiko.canary.utils.VersionFactory;

/**
 * Canary Metrics
 *
 * @author Leonard Woo
 */
public class MetricsService {

  public MetricsService() {
  }

  public Metric getMetrics() {
    Metric metric = new Metric();

    metric.setVersion(VersionFactory.VERSION);

    MXBean mxBean = new MXBean();
    mxBean.setRuntimeMXBean(JmxMetricsUtil.getRuntime());
    mxBean.setClazzLoadingMXBean(JmxMetricsUtil.getClazzLoading());
    mxBean.setThreadMXBean(JmxMetricsUtil.getThread());
    metric.setMxBean(mxBean);

    return metric;
  }

}
