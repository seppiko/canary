/*
 * Copyright 2023 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.seppiko.canary.services;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.StringJoiner;
import java.util.Vector;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;
import org.seppiko.canary.configures.APIEntity;
import org.seppiko.canary.configures.CanaryConfiguration;
import org.seppiko.canary.models.ResponseMessageEntity;
import org.seppiko.canary.models.ResponseStatusEntity;
import org.seppiko.canary.models.ResponseTimelineEntity;
import org.seppiko.canary.models.mastodon.Account;
import org.seppiko.canary.models.mastodon.AccountLookup;
import org.seppiko.canary.models.mastodon.TootLookup;
import org.seppiko.canary.utils.HttpUtil;
import org.seppiko.canary.utils.JsonUtil;
import org.seppiko.canary.utils.MastodonUrlUtil;
import org.seppiko.canary.utils.MastodonUtil;
import org.seppiko.commons.logging.Logging;
import org.seppiko.commons.logging.LoggingFactory;
import org.seppiko.commons.utils.CharUtil;
import org.seppiko.commons.utils.CollectionUtil;
import org.seppiko.commons.utils.NumberUtil;
import org.seppiko.commons.utils.StringUtil;

/**
 * @author Leonard Woo
 */
public class MastodonService {

  private static final CanaryConfiguration config = CanaryConfiguration.getInstance();
  private static final APIEntity MASTODON = config.getMastodon();
  private static final int MULTIPLE_GET_INTERVAL_SECONDS;

  static {
    if (MASTODON != null) {
      MULTIPLE_GET_INTERVAL_SECONDS = MASTODON.getMultipleIntervalsSeconds();
    } else {
      MULTIPLE_GET_INTERVAL_SECONDS = 0;
    }
  }

  private final Logging log = LoggingFactory.getLogging("mastodon");

  private final ArrayList<ResponseMessageEntity<?>> errors = new ArrayList<>();

  private final Vector<ResponseStatusEntity> toots;

  private final ResponseTimelineEntity timeline;

  private boolean needPreview;

  public MastodonService() {
    toots = new Vector<>();
    timeline = new ResponseTimelineEntity();
  }

  private boolean nonConfig() {
    if (MASTODON == null || StringUtil.isNullOrEmpty(MASTODON.getToken())) {
      addError(400, "Not set Mastodon Token");
      return true;
    }
    return false;
  }

  public ArrayList<ResponseMessageEntity<?>> getErrors() {
    try {
      return (ArrayList<ResponseMessageEntity<?>>) CollectionUtil.clone(errors, ArrayList::new);
    } finally {
      errors.clear();
    }
  }

  private void addError(int code, String msg) {
    errors.add(new ResponseMessageEntity<>(code, msg));
  }

  public ArrayList<ResponseStatusEntity> getTootList() {
    try {
      return new ArrayList<>(CollectionUtil.clone(toots, Vector::new));
    } finally {
      toots.clear();
    }
  }

  public ResponseTimelineEntity getTimeline() {
    return timeline;
  }

  public void clearTimeline() {
    timeline._clear();
  }

  public void setNeedPreview(boolean needPreview) {
    this.needPreview = needPreview;
  }

  public boolean getTootMedias(final String tootIds) {
    if (nonConfig()) {
      return false;
    }
    if (tootIds.indexOf(CharUtil.COMMA) < 1) {
      return getTootMedia(tootIds);
    }

    String[] _tootIds = tootIds.split(",");
    for(int i = 0; i < _tootIds.length; i++) {
      if (!getTootMedia(_tootIds[i])) {
        break;
      }
      if (i < (_tootIds.length - 1)) {
        try {
          TimeUnit.SECONDS.sleep(MULTIPLE_GET_INTERVAL_SECONDS);
        } catch (InterruptedException ignored) {
        }
      }
    }
    return true;
  }

  private boolean getTootMedia(final String tootId) {
    StringBuilder urlBuilder = new StringBuilder();
    urlBuilder.append(MastodonUrlUtil.MASTODON_DOMAIN);
    urlBuilder.append(String.format(MastodonUrlUtil.MASTODON_SINGLE_STATUS, tootId));

    ExecutorService executor = Executors.newFixedThreadPool(1);
    try {
      Future<Boolean> future = executor.submit(() -> {
        var resp = HttpUtil.get(urlBuilder.toString(), MASTODON);
        if (resp == null) {
          addError(408, "API request timeout.");
          return false;
        }
        String respJson = resp.body();
        log.info(resp.statusCode() + " " + respJson);

        TootLookup lookup = JsonUtil.fromJson(respJson, TootLookup.class);
        if (lookup == null) {
          return false;
        }
        if (lookup.getError() != null) {
          addError(404, lookup.getError());
          return false;
        }

        ResponseStatusEntity respToot = MastodonUtil.parserToot(lookup, needPreview);
        toots.add(respToot);
        showToot();

        return true;
      });

      return future.get();
    } catch (Exception ex) {
      log.error("Toot media task execute exception: " + ex.getMessage());
      return false;
    } finally {
      executor.shutdown();
    }
  }

  public boolean getTimelineMedias(final String username, final int limit, final String maxId,
      final String sinceId) {
    if (nonConfig()) {
      return false;
    }
    if (limit > 0 && !NumberUtil.between(limit, 1, 40)) {
      return false;
    }

    try {
      CompletableFuture<Boolean> futureAsync = CompletableFuture.supplyAsync(() -> {
        Account account = filterAccount(accountLookup(username), username);
        if (account == null) {
          return false;
        }

        boolean flag = getTimeline(account.getId(), limit, maxId, sinceId);
        log.info(String.format("Timeline %d found", limit));
        showTimeline();
        return flag;
      });
      return futureAsync.get();
    } catch (ExecutionException | InterruptedException | RuntimeException ex) {
      log.error("Timeline media task execute exception: " + ex.getMessage());
    }
    return false;
  }

  public boolean getAllTimelineMedias(final String username) {
    if (nonConfig()) {
      return false;
    }
    try {
      CompletableFuture<Boolean> futureAsync = CompletableFuture.supplyAsync(() -> {
        Account account = filterAccount(accountLookup(username), username);
        if (account == null) {
          return false;
        }

        boolean flag = getAllTimeline(account.getId());
        log.info(String.format("Timeline %d found", timeline.getCount()));
        showTimeline();
        return flag;
      });
      return futureAsync.get();
    } catch (ExecutionException | InterruptedException | RuntimeException ex) {
      log.error("Timeline media task execute exception: " + ex.getMessage());
    }
    return false;
  }

  private boolean getTimeline(final String userID, final int limit, final String maxId,
      final String sinceId) {
    if (MastodonUtil.accountStatusesIdCheck(maxId, sinceId) < 0) {
      return false;
    }
    ArrayList<TootLookup> tl = getTimeline0(userID, limit, maxId, sinceId);
    if (tl == null) {
      return false;
    }
    timeline._clear();
    timeline.addAll(MastodonUtil.parserTimeline(tl, needPreview));
    return true;
  }

  private boolean getAllTimeline(String userID) {
    ArrayList<TootLookup> allTl = new ArrayList<>();
    String maxId = "";
    do {
      ArrayList<TootLookup> tl = getTimeline0(userID, 40, maxId, "");
      if (tl == null) {
        return false;
      }
      if (tl.size() == 0) {
        break;
      }

      // since id to max id
      tl.sort(Comparator.comparingLong(toot -> Long.parseLong(toot.getId())));

      allTl.addAll(tl);
      maxId = tl.get(tl.size() - 1).getId();
      try {
        TimeUnit.SECONDS.sleep(MULTIPLE_GET_INTERVAL_SECONDS);
      } catch (InterruptedException ignored) {
      }
    } while (true);

    timeline._clear();
    timeline.addAll(MastodonUtil.parserTimeline(allTl, needPreview));
    return true;
  }

  private AccountLookup accountLookup(String username) {
    if (username.startsWith("@")) {
      username = username.substring(1);
    }
    StringBuilder urlBuilder = new StringBuilder(MastodonUrlUtil.MASTODON_DOMAIN);
    urlBuilder.append(MastodonUrlUtil.MASTODON_SEARCH);
    StringJoiner urlJoiner = new StringJoiner("&", "?", "");
    urlJoiner.add("q=" + username);
    urlJoiner.add("type=accounts");
    urlJoiner.add("limit=5");
    urlBuilder.append(urlJoiner);

    var resp = HttpUtil.get(urlBuilder.toString(), MASTODON);
    if (resp == null) {
      addError(408, "API request timeout.");
      return null;
    }
    final String respJson = resp.body();
    log.info(resp.statusCode() + " " + respJson);

    return JsonUtil.fromJson(respJson, AccountLookup.class);
  }

  private Account filterAccount(AccountLookup lookup, String username) {
    if (lookup == null) {
      return null;
    }
    for (Account account: lookup.getAccounts()) {
      if (username.equals(account.getUsername()) || username.equals(account.getAcct())) {
        return account;
      }
    }
    return null;
  }

  // uncheck cause toot list
  @SuppressWarnings("unchecked")
  private ArrayList<TootLookup> getTimeline0(final String userID, final int limit, final String maxId,
      final String sinceId) {
    StringBuilder urlBuilder = new StringBuilder(MastodonUrlUtil.MASTODON_DOMAIN);
    urlBuilder.append(String.format(MastodonUrlUtil.MASTODON_TIMELINE, userID));
    urlBuilder.append("?");
//    urlBuilder.append("local=true");
//    urlBuilder.append("&remote=true");
//    urlBuilder.append("&only_media=true");
    if (StringUtil.hasText(maxId)) {
      urlBuilder.append("&max_id=");
      urlBuilder.append(maxId);
    }
    if (StringUtil.hasText(sinceId)) {
      urlBuilder.append("&since_id=");
      urlBuilder.append(sinceId);
    }
    if (limit > 1) {
      urlBuilder.append("&limit=");
      urlBuilder.append(limit);
    }

    var resp = HttpUtil.get(urlBuilder.toString(), MASTODON);
    if (resp == null) {
      addError(408, "API request timeout.");
      return null;
    }
    String respJson = resp.body();
    log.info(resp.statusCode() + " " + respJson);

    return (ArrayList<TootLookup>) JsonUtil.fromJson(respJson, ArrayList.class);
  }

  private void showToot() {
    toots.forEach(toot -> {
      log.info(toot.toString());
      var medias = toot.medias();
      if (medias != null) {
        medias.forEach(respMedia -> {
          log.info(respMedia.toString());
        });
        log.info(String.format("%d media found", medias.size()));
      }
    });
  }

  private void showTimeline() {
    timeline.getStatuses().forEach(respToot -> {
      log.info(respToot.toString());
      var medias = respToot.medias();
      if (medias != null) {
        medias.forEach(respMedia -> {
          log.info(respMedia.toString());
        });
        log.info(String.format("%d media found", medias.size()));
      }
    });
    log.info(String.format("Timeline between %s to %s", timeline.getUntilId(), timeline.getSinceId()));
  }
}
