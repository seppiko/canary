/*
 * Copyright 2023 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.seppiko.canary.services;

import io.minio.errors.MinioException;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.IOException;
import java.security.GeneralSecurityException;
import java.util.ArrayList;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicBoolean;
import org.seppiko.canary.configures.APIType;
import org.seppiko.canary.configures.CanaryConfiguration;
import org.seppiko.canary.configures.OSEntity;
import org.seppiko.canary.configures.StorageEntity;
import org.seppiko.canary.models.ResponseMediaEntity;
import org.seppiko.canary.models.ResponseStatusEntity;
import org.seppiko.canary.utils.FileUtil;
import org.seppiko.canary.utils.HttpUtil;
import org.seppiko.canary.utils.MastodonUtil;
import org.seppiko.canary.utils.S3Util;
import org.seppiko.canary.utils.Twitter2Util;
import org.seppiko.commons.logging.Logging;
import org.seppiko.commons.logging.LoggingFactory;
import org.seppiko.commons.utils.CharUtil;
import org.seppiko.commons.utils.StringUtil;

/**
 * Canary media download
 *
 * @author Leonard Woo
 */
public class MediaService {

  private static final CanaryConfiguration config = CanaryConfiguration.getInstance();
  private static final int MULTIPLE_DOWNLOAD_INTERVAL_SECONDS;
  private static final String type;
  private static String pathname;
  private static final OSEntity oss;

  static {
    MULTIPLE_DOWNLOAD_INTERVAL_SECONDS = config.getDownload().getMultipleIntervalsSeconds();
    StorageEntity storage = config.getStorage();
    type = storage.getType();
    pathname = storage.getPathname();
    oss = storage.getOss();
  }

  private final Logging log = LoggingFactory.getLogging(this.getClass());

  private S3Util s3;

  public MediaService() {
    if ("file".equals(type)) {
      if (!pathname.endsWith(CanaryConfiguration.FILE_SEPARATOR)) {
        pathname = pathname + CanaryConfiguration.FILE_SEPARATOR;
      }
    } else if ("s3".equals(type)) {
      s3 = new S3Util(oss.getUrl(), oss.getAccessKey(), oss.getSecretKey(), oss.getRegion());
      try {
        s3.createBucket(oss.getBucketName());
      } catch (MinioException | IOException | GeneralSecurityException e) {
        log.error("", e);
      }
    }
  }

  public boolean download(final String[] urls) {
    boolean lastFlag = false;
    for (int i = 0; i < urls.length; i++) {
      try {
        int finalI = i;
        CompletableFuture<Boolean> futureAsync = CompletableFuture.supplyAsync(() -> download0(urls[finalI]));
        lastFlag = futureAsync.get();
        if (i != (urls.length - 1)) {
          TimeUnit.SECONDS.sleep(MULTIPLE_DOWNLOAD_INTERVAL_SECONDS);
        }
      } catch (RuntimeException | InterruptedException e) {
        log.error("Downloads exception: " + e.getMessage());
        lastFlag = false;
      } catch (ExecutionException e) {
        log.warn("Downloads async thread exception: " + e.getMessage());
        lastFlag = false;
      }
    }
    return lastFlag;
  }

  private boolean download0(final String url) {
    if (StringUtil.nonText(url)) {
      return false;
    }

    StringBuilder filename = new StringBuilder();
    filename.append(pathname);
    filename.append(FileUtil.getFilename(url));

    if (type.equals("file")) {
      File path = new File(pathname);
      File file = new File(filename.toString());
      try {
        if (!path.exists()) {
          if (!path.mkdirs()) {
            return false;
          }
        }
        if (file.exists()) {
          log.warn("File " + pathname + " already exists.");
          return true;
        }
      } catch (SecurityException ex) {
        log.error("", ex);
        return false;
      }
    } else if (type.equals("s3")) {
      if (s3.existObjectName(pathname) ) {
        log.warn("Object " + pathname + " already exists.");
        return true;
      }
    }

    byte[] data = HttpUtil.download(url);
    boolean flag = false;
    if (data != null && data.length > 0) {
      if (!FileUtil.includeExt(filename.toString())) {
        String type = HttpUtil.getContentType();
        filename.append(FileUtil.getFileExt(type));
      }

      if (type.equals("file")) {
        flag = FileUtil.saveFile(data, filename.toString());
      } else if (type.equals("s3")) {
        try {
          String pathname = filename.toString();
          if (pathname.indexOf(CharUtil.REVERSE_SOLIDUS) >= 0) {
            pathname = pathname.replace("\\", "/");
          }
          if (pathname.startsWith("/")) {
            pathname = pathname.substring(1);
          }

          s3.uploadStream(pathname, new ByteArrayInputStream(data), data.length);
          flag = true;
        } catch (MinioException | IOException | GeneralSecurityException e) {
          log.warn("file upload failed. " + e.getMessage());
        }
      }
    }
    log.info(String.format("Media download %s: %s", (flag ? "Success" : "Failed"), url));
    return flag;
  }

  public boolean downloadMedias(final ArrayList<ResponseStatusEntity> statuses, final APIType type) {
    try {
      ExecutorService executor = Executors.newFixedThreadPool(1);
      Future<Boolean> future = executor.submit(() -> {
        AtomicBoolean flag = new AtomicBoolean(false);
        statuses.forEach(status -> {
          ArrayList<ResponseMediaEntity> mediaUrls = status.medias();
          ArrayList<String> downloadUrls = new ArrayList<>();
          if (mediaUrls != null && !mediaUrls.isEmpty()) {
            mediaUrls.forEach(m -> {
              switch (type) {
                case TWITTER -> {
                  downloadUrls.add(Twitter2Util.tweetMediaUrlReplace(m.type(), m.url()));
                }
                case MASTODON -> {
                  downloadUrls.add(MastodonUtil.tootMediaUrlReplace(m.type(), m.url()));
                }
              }
            });
          }

          String[] urls = downloadUrls.toArray(String[]::new);
          if (urls.length > 0) {
            flag.set(download(urls));
            if (flag.get()) {
              log.info(String.format("Download %s medias done", status.id()));
            }
          }
        });
        return flag.get();
      });
      return future.get();
    } catch (RuntimeException | ExecutionException e) {
      log.error("Download exception: " + e.getMessage());
    } catch (InterruptedException e) {
      log.error("Download thread exception: " + e.getMessage());
    }
    return false;
  }
}
