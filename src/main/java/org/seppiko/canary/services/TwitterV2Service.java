/*
 * Copyright 2023 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.seppiko.canary.services;

import java.util.ArrayList;
import java.util.Vector;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;
import org.seppiko.canary.configures.APIEntity;
import org.seppiko.canary.configures.CanaryConfiguration;
import org.seppiko.canary.models.ResponseMessageEntity;
import org.seppiko.canary.models.ResponseTimelineEntity;
import org.seppiko.canary.models.ResponseStatusEntity;
import org.seppiko.canary.models.twitterv2.Timeline;
import org.seppiko.canary.models.twitterv2.TweetLookup;
import org.seppiko.canary.models.twitterv2.TweetsLookup;
import org.seppiko.canary.models.twitterv2.UserLookup;
import org.seppiko.canary.utils.HttpUtil;
import org.seppiko.canary.utils.JsonUtil;
import org.seppiko.canary.utils.Twitter2Util;
import org.seppiko.canary.utils.TwitterUrlUtil;
import org.seppiko.commons.logging.Logging;
import org.seppiko.commons.logging.LoggingFactory;
import org.seppiko.commons.utils.CharUtil;
import org.seppiko.commons.utils.CollectionUtil;
import org.seppiko.commons.utils.NumberUtil;
import org.seppiko.commons.utils.StringUtil;

/**
 * Twitter API 2.0
 *
 * @author Leonard Woo
 */
public class TwitterV2Service {

  private static final CanaryConfiguration config = CanaryConfiguration.getInstance();
  private static final APIEntity TWITTER = config.getTwitter();
  private static final int MULTIPLE_GET_INTERVAL_SECONDS;

  static {
    if (TWITTER != null) {
      MULTIPLE_GET_INTERVAL_SECONDS = TWITTER.getMultipleIntervalsSeconds();
    } else {
      MULTIPLE_GET_INTERVAL_SECONDS = 0;
    }

  }

  private final Logging log = LoggingFactory.getLogging("twitter2");

  private final Vector<ResponseStatusEntity> tweetList;

  private final ResponseTimelineEntity timeline;

  private final ArrayList<ResponseMessageEntity<?>> errors = new ArrayList<>();

  private boolean needPreview;

  public TwitterV2Service() {
    tweetList = new Vector<>();
    timeline = new ResponseTimelineEntity();
  }

  private boolean nonConfig() {
    if (TWITTER == null || StringUtil.isNullOrEmpty(TWITTER.getToken())) {
      addError(400, "Not set Twitter Token");
      return true;
    }
    return false;
  }

  public ArrayList<ResponseStatusEntity> getTweetV2List() {
    try {
      return new ArrayList<>(CollectionUtil.clone(tweetList, Vector::new));
    } finally {
      tweetList.clear();
    }
  }

  public ArrayList<ResponseMessageEntity<?>> getErrors() {
    try {
      return (ArrayList<ResponseMessageEntity<?>>) CollectionUtil.clone(errors, ArrayList::new);
    } finally {
      errors.clear();
    }
  }

  private void addError(int code, String msg) {
    errors.add(new ResponseMessageEntity<>(code, msg));
  }

  public ResponseTimelineEntity getTimeline() {
    return timeline;
  }

  public void clearTimeline() {
    timeline._clear();
  }

  public void setNeedPreview(boolean needPreview) {
    this.needPreview = needPreview;
  }

  private UserLookup getLookup(String username) {
    String url = TwitterUrlUtil.TWITTER_API + String.format(TwitterUrlUtil.TWITTER_API2_LOOKUP, username);

    var resp = HttpUtil.get(url, TWITTER);
    if (resp == null) {
      addError(408, "API request timeout.");
      return null;
    }
    final String respJson = resp.body();
    log.info(resp.statusCode() + " " + respJson);

    UserLookup lookup = JsonUtil.fromJson(respJson, UserLookup.class);
    if (lookup == null) {
      return null;
    }
    if (lookup.getErrors() != null) {
      lookup.getErrors().forEach(error -> {
        addError(Twitter2Util.parserTweetErrorType(error.getType()),
                error.getDetail());
      });
      return null;
    }
    return lookup;
  }

  public boolean getTweetMedia(final String tweetId, boolean maxVideo) {
    if (nonConfig()) {
      return false;
    }
    if (tweetId.indexOf(CharUtil.COMMA) >= 0) {
      return getTweetMedias(tweetId, maxVideo);
    }

    StringBuilder urlBuilder = new StringBuilder(TwitterUrlUtil.TWITTER_API);
    urlBuilder.append(String.format(TwitterUrlUtil.TWITTER_API2_TWEET, tweetId));
    urlBuilder.append("?");
    urlBuilder.append(TwitterUrlUtil.TWITTER_API2_ALL_FIELDS);

    ExecutorService executor = Executors.newFixedThreadPool(1);
    try {
      Future<Boolean> future = executor.submit(() -> {
        var resp = HttpUtil.get(urlBuilder.toString(), TWITTER);
        if (resp == null) {
          addError(408, "API request timeout.");
          return false;
        }
        String respJson = resp.body();
        log.info(resp.statusCode() + " " + respJson);

        TweetLookup lookup = JsonUtil.fromJson(respJson, TweetLookup.class);
        if (lookup == null) {
          return false;
        }
        if (lookup.getErrors() != null) {
          lookup.getErrors().forEach(error -> {
            addError(Twitter2Util.parserTweetErrorType(error.getType()),
                    error.getDetail());
          });
          return false;
        }

        ResponseStatusEntity respTweet = Twitter2Util.parserTweetV2(lookup, maxVideo, needPreview);
        tweetList.add(respTweet);
        showTweets();

        return true;
      });

      return future.get();
    } catch (Exception ex) {
      log.error("Tweet media task execute exception: " + ex.getMessage());
      return false;
    } finally {
      executor.shutdown();
    }
  }

  public boolean getTweetMedias(final String tweetIds, boolean maxVideo) {
    StringBuilder urlBuilder = new StringBuilder(TwitterUrlUtil.TWITTER_API);
    urlBuilder.append(TwitterUrlUtil.TWITTER_API2_TWEETS);
    urlBuilder.append("?ids=").append(tweetIds);
    urlBuilder.append("&");
    urlBuilder.append(TwitterUrlUtil.TWITTER_API2_ALL_FIELDS);

    ExecutorService executor = Executors.newFixedThreadPool(1);
    try {
      Future<Boolean> future = executor.submit(() -> {
        var resp = HttpUtil.get(urlBuilder.toString(), TWITTER);
        if (resp == null) {
          addError(408, "API request timeout.");
          return false;
        }
        String respJson = resp.body();
        log.info(resp.statusCode() + " " + respJson);

        TweetsLookup lookup = JsonUtil.fromJson(respJson, TweetsLookup.class);
        if (lookup == null) {
          return false;
        }
        if (lookup.getErrors() != null) {
          lookup.getErrors().forEach(error -> {
            addError(Twitter2Util.parserTweetErrorType(error.getType()),
                    error.getDetail());
          });
          return false;
        }

        ArrayList<ResponseStatusEntity> respTweets = Twitter2Util.parserTweetsV2(lookup, maxVideo, needPreview);
        tweetList.addAll(respTweets);
        showTweets();

        if (respTweets.size() > 1) {
          log.info(String.format("Tweets %d done", respTweets.size()));
        }

        return true;
      });

      return future.get();
    } catch (Exception ex) {
      log.error("Tweets media task execute exception: " + ex.getMessage());
      return false;
    } finally {
      executor.shutdown();
    }
  }

  private Timeline getTimeline0(String userID, int count, final String untilId, final String sinceId, boolean getTweetDetail) {
    StringBuilder urlBuilder = new StringBuilder(TwitterUrlUtil.TWITTER_API);
    urlBuilder.append(String.format(TwitterUrlUtil.TWITTER_API2_TIMELINE, userID));
    urlBuilder.append("?");
    urlBuilder.append(TwitterUrlUtil.TWITTER_API2_TIMELINE_OPTIONS);
    if (count > 4) {
      urlBuilder.append("&max_results=");
      urlBuilder.append(count);
    }
    if (StringUtil.hasText(untilId)) {
      urlBuilder.append("&until_id=");
      urlBuilder.append(untilId);
    }
    if (StringUtil.hasText(sinceId)) {
      urlBuilder.append("&since_id=");
      urlBuilder.append(sinceId);
    }
    if (getTweetDetail) {
      urlBuilder.append("&");
      urlBuilder.append(TwitterUrlUtil.TWITTER_API2_ALL_FIELDS);
    }

    var resp = HttpUtil.get(urlBuilder.toString(), TWITTER);
    if (resp == null) {
      addError(408, "API request timeout.");
      return null;
    }
    String respJson = resp.body();
    log.info(resp.statusCode() + " " + respJson);

    return JsonUtil.fromJson(respJson, Timeline.class);
  }

  private boolean getTimeline(String userID, int count, final String untilId, final String sinceId,
      boolean getTweetDetail, boolean maxVideo) {
    if (Twitter2Util.timelineIdCheck(untilId, sinceId) <= 0 ) {
      return false;
    }
    Timeline tl = getTimeline0(userID, count, untilId, sinceId, getTweetDetail);
    if (tl == null) {
      return false;
    }
    if (tl.getErrors() != null) {
      if (tl.getType() == null || "".equals(tl.getType())) {
        tl.getErrors().forEach(error -> {
          addError(Twitter2Util.parserTweetErrorType(error.getType()),
                  error.getDetail());
        });
      } else {
        addError(Twitter2Util.parserTweetErrorType(tl.getType()),
                tl.getDetail());
      }
      if (tl.getData() == null) {
        return false;
      }
    }
    if (tl.getMeta().getResultCount() == 0) {
      return true;
    }

    timeline._clear();
    timeline.addAll(Twitter2Util.parserTimelineV2(tl, maxVideo, needPreview));
    return true;
  }

  private boolean getAllTimeline(String userID, boolean getTweetDetail, boolean maxVideo) {
    Timeline allTl = new Timeline();
    String untilId = "";
    do {
      final Timeline tl = getTimeline0(userID, 100, untilId, "", getTweetDetail);
      if (tl == null) {
        return false;
      }
      if (tl.getErrors() != null) {
        if (tl.getType() == null || "".equals(tl.getType())) {
          tl.getErrors().forEach(error -> {
            addError(Twitter2Util.parserTweetErrorType(error.getType()),
                    error.getDetail());
          });
        } else {
          addError(Twitter2Util.parserTweetErrorType(tl.getType()),
                  tl.getDetail());
        }
        if (tl.getData() == null) {
          return false;
        }
      }
      if (tl.getMeta().getResultCount() == 0) {
        break;
      }

      allTl = Twitter2Util.mergeTimeline(allTl.clone(), tl);
      untilId = tl.getMeta().getOldestId();
      try {
        TimeUnit.SECONDS.sleep(MULTIPLE_GET_INTERVAL_SECONDS);
      } catch (InterruptedException ignored) {
      }
    } while (true);

    timeline._clear();
    timeline.addAll(Twitter2Util.parserTimelineV2(allTl, maxVideo, needPreview));
    return true;
  }

  public boolean getTimelineMedias(final String username, final int count, final String untilId,
      final String sinceId, boolean maxVideo) {
    if (nonConfig()) {
      return false;
    }
    if (count > 0 && !NumberUtil.between(count, 5, 100)) {
      return false;
    }

    try {
      CompletableFuture<Boolean> futureAsync = CompletableFuture.supplyAsync(() -> {
        UserLookup ul = getLookup(username);
        if (ul == null) {
          return false;
        }

        boolean flag = getTimeline(ul.getData().getId(), count, untilId, sinceId, true, maxVideo);
        log.info(String.format("Timeline %d found", count));
        showTimeline();
        return flag;
      });
      return futureAsync.get();
    } catch (ExecutionException | InterruptedException | RuntimeException ex) {
      log.error("Timeline media task execute exception: " + ex.getMessage());
      return false;
    }
  }

  public boolean getAllTimelineMedias(String username, boolean maxVideo) {
    if (nonConfig()) {
      return false;
    }
    try {
      CompletableFuture<Boolean> futureAsync = CompletableFuture.supplyAsync(() -> {
        UserLookup ul = getLookup(username);
        if (ul == null) {
          return false;
        }

        boolean flag = getAllTimeline(ul.getData().getId(), true, maxVideo);
        log.info(String.format("Timeline %d found", timeline.getCount()));
        showTimeline();
        return flag;
      });
      return futureAsync.get();
    } catch (ExecutionException | InterruptedException | RuntimeException ex) {
      log.error("Timelines media task execute exception: " + ex.getMessage());
      return false;
    }
  }

  private void showTweets() {
    tweetList.forEach(respTweet -> {
      log.info(respTweet.toString());
      var medias = respTweet.medias();
      if (medias != null) {
        medias.forEach(respMedia -> {
          log.info(respMedia.toString());
        });
        log.info(String.format("%d media found", medias.size()));
      }
    });
  }

  private void showTimeline() {
    timeline.getStatuses().forEach(respTweet -> {
      log.info(respTweet.toString());
      var medias = respTweet.medias();
      if (medias != null) {
        medias.forEach(respMedia -> {
          log.info(respMedia.toString());
        });
        log.info(String.format("%d media found", medias.size()));
      }
    });
    log.info(String.format("Timeline between %s to %s", timeline.getUntilId(), timeline.getSinceId()));
  }
}
