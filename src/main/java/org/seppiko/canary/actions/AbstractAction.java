/*
 * Copyright 2023 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.seppiko.canary.actions;

import io.undertow.Version;
import jakarta.servlet.ServletException;
import jakarta.servlet.ServletInputStream;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;

import java.util.Objects;
import org.seppiko.canary.configures.CanaryConfiguration;
import org.seppiko.canary.models.ResponseEntity;
import org.seppiko.canary.models.ResponseMessageEntity;
import org.seppiko.canary.utils.JsonUtil;
import org.seppiko.commons.utils.Environment;
import org.seppiko.commons.utils.http.HttpStatus;
import org.seppiko.commons.utils.http.MediaType;


import java.io.IOException;
import java.io.OutputStream;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;

/**
 * Http servlet abstract action
 *
 * @author Leonard Woo
 */
public abstract class AbstractAction extends HttpServlet {

  protected abstract ResponseEntity contentHandleExecution(HttpServletRequest request) throws ServletException, IOException;

  protected ResponseEntity methodNotAllowed() {
    ArrayList<ResponseMessageEntity<?>> errorList = new ArrayList<>();
    errorList.add(new ResponseMessageEntity<>(405, "Method Not Allowed."));
    return sendJson(HttpStatus.METHOD_NOT_ALLOWED, new ResponseMessageEntity<>(errorList));
  }

  protected ResponseEntity badRequest() {
    return sendJson(HttpStatus.OK, new ResponseMessageEntity<>(400, "Bad request"));
  }

  protected <T> ResponseEntity ok(String msg) {
    return ok(msg, null);
  }

  protected <T> ResponseEntity ok(String msg, T t) {
    return sendJson(HttpStatus.OK, new ResponseMessageEntity<>(200, msg, t));
  }

  protected ResponseEntity failed(String msg) {
    return failed(msg, null);
  }

  protected <T> ResponseEntity failed(String msg, T t) {
    return sendJson(HttpStatus.OK, new ResponseMessageEntity<>(403, msg, t));
  }

  protected <T> ResponseEntity sendJson(HttpStatus status, T resp) {
    return new ResponseEntity(status, MediaType.APPLICATION_JSON, JsonUtil.toJson(resp).getBytes(StandardCharsets.UTF_8));
  }

  protected byte[] getBody(ServletInputStream in, int totalLen) throws IOException {
    if (totalLen <= 0) {
      return Environment.EMPTY_BYTE_ARRAY;
    }
    byte[] ba = new byte[totalLen];
    int tn = in.read(ba, 0, totalLen);
    if (tn < 0 || tn != totalLen) {
      return Environment.EMPTY_BYTE_ARRAY;
    }
    return ba;
  }

  // action service
  /** {@inheritDoc} */
  @Override
  protected void service(HttpServletRequest req, HttpServletResponse resp)
          throws ServletException, IOException {
    ResponseEntity chxResp = contentHandleExecution(req);
    if (Objects.isNull(chxResp)) {
      throw new ServletException("Action methods is null");
    }
    resp.setStatus(chxResp.statusCode());
    resp.setCharacterEncoding(StandardCharsets.UTF_8.name());
    resp.addHeader("Server", "Undertow/" + Version.getVersionString());
    resp.addHeader("X-Powered-By", CanaryConfiguration.UA);
    resp.setContentType(chxResp.typeValue());
    byte[] body = chxResp.body();
    resp.setContentLength(body.length);
    OutputStream os = resp.getOutputStream();
    os.write(body);
    os.flush();
    os.close();
  }
}
