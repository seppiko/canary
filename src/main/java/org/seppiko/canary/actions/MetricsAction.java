/*
 * Copyright 2023 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.seppiko.canary.actions;

import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServletRequest;
import org.seppiko.canary.models.ResponseEntity;
import org.seppiko.canary.services.MetricsService;
import org.seppiko.commons.utils.http.HttpMethod;

import java.io.IOException;

/**
 * Metrics action
 * @author Leonard Woo
 */
public class MetricsAction extends AbstractAction {

  private MetricsService service = new MetricsService();

  @Override
  protected ResponseEntity contentHandleExecution(HttpServletRequest request)
          throws ServletException, IOException {
    if (!request.getMethod().equals(HttpMethod.GET.name())) {
      return methodNotAllowed();
    }
    return ok("ok", service.getMetrics());
  }
}
