/*
 * Copyright 2023 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.seppiko.canary.actions;

import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServletRequest;
import org.seppiko.canary.configures.APIType;
import org.seppiko.canary.models.RequestTwitterJsonEntity;
import org.seppiko.canary.models.ResponseEntity;
import org.seppiko.canary.models.ResponseTimelineEntity;
import org.seppiko.canary.services.MediaService;
import org.seppiko.canary.services.TwitterV2Service;
import org.seppiko.canary.utils.CanaryUtil;
import org.seppiko.canary.utils.JsonUtil;
import org.seppiko.commons.logging.Logging;
import org.seppiko.commons.logging.LoggingFactory;
import org.seppiko.commons.utils.StringUtil;
import org.seppiko.commons.utils.http.HttpMethod;

import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.util.concurrent.atomic.AtomicBoolean;

/**
 * @author Leonard Woo
 */
public class Twitter2Action extends AbstractAction {

  private final Logging log = LoggingFactory.getLogging(this.getClass());

  private TwitterV2Service service = new TwitterV2Service();

  private MediaService serviceM = new MediaService();

  @Override
  protected ResponseEntity contentHandleExecution(HttpServletRequest request)
          throws ServletException, IOException {
    if (request.getMethod().equals(HttpMethod.POST.name()) && request.getContentLength() > 0) {
      String encoding = request.getCharacterEncoding();
      if (!StringUtil.hasText(encoding)) {
        encoding = StandardCharsets.UTF_8.name();
      }

      String body = new String(getBody(request.getInputStream(), request.getContentLength()), encoding);
      log.info(body);
      if (StringUtil.nonText(body)) {
        return badRequest();
      }

      RequestTwitterJsonEntity reqJson = JsonUtil.fromJson(body, RequestTwitterJsonEntity.class);
      if (reqJson == null) {
        return badRequest();
      }

      String tweetIds = CanaryUtil.getOrElse(reqJson.tweetIds(),"");
      String username = CanaryUtil.getOrElse(reqJson.username(), "");
      int count = CanaryUtil.getOrElse(reqJson.count(), -1);
      String untilId = CanaryUtil.getOrElse(reqJson.untilId(), "");
      String sinceId = CanaryUtil.getOrElse(reqJson.sinceId(), "");
      service.setNeedPreview(CanaryUtil.getOrElse(reqJson.preview(), true));
      boolean onlyMaxMedia = CanaryUtil.getOrElse(reqJson.onlyMaxMedia(), false);
      boolean downloadMedia = CanaryUtil.getOrElse(reqJson.downloadMedia(), false);
      service.clearTimeline();

      ResponseTimelineEntity timeline = new ResponseTimelineEntity();
      AtomicBoolean flag = new AtomicBoolean(false);

      if (StringUtil.hasLength(tweetIds) && !tweetIds.isBlank()) {
        if (service.getTweetMedia(tweetIds, onlyMaxMedia)) {
          timeline.setStatuses(service.getTweetV2List());
          flag.set(true);
        }
      }

      if (StringUtil.hasLength(username) && !username.isBlank() && count > 0) {
        if (service.getTimelineMedias(username, count, untilId, sinceId, onlyMaxMedia)) {
          timeline.addAll(service.getTimeline());
          flag.set(true);
        }
      }

      if (StringUtil.hasLength(username) && !username.isBlank() && count <= 0 && untilId.isEmpty()) {
        if (service.getAllTimelineMedias(username, onlyMaxMedia)) {
          timeline.addAll(service.getTimeline());
          flag.set(true);
        }
      }

      if (downloadMedia) {
        flag.set(serviceM.downloadMedias(timeline.getStatuses(), APIType.TWITTER));
        timeline._onlyClear();
      }

      if (flag.get()) {
        if (timeline._isEmpty()) {
          return ok("Successful");
        }
        return ok("Successful", timeline);
      }
      return failed("Failed", service.getErrors());
    }
    return methodNotAllowed();
  }

}
