/*
 * Copyright 2023 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.seppiko.canary.actions;

import io.undertow.server.HttpServerExchange;
import io.undertow.servlet.api.ExceptionHandler;
import jakarta.servlet.ServletRequest;
import jakarta.servlet.ServletResponse;
import java.io.IOException;
import java.io.OutputStream;
import java.nio.charset.StandardCharsets;
import org.seppiko.canary.models.ResponseEntity;
import org.seppiko.canary.models.ResponseMessageEntity;
import org.seppiko.canary.utils.JsonUtil;
import org.seppiko.commons.logging.Logging;
import org.seppiko.commons.logging.LoggingFactory;
import org.seppiko.commons.utils.http.HttpStatus;
import org.seppiko.commons.utils.http.MediaType;

/**
 * Error action
 *
 * @author Leonard Woo
 */
public class ErrorAction implements ExceptionHandler {

  private final Logging logger = LoggingFactory.getLogging(this.getClass());

  public ExceptionHandler get() {
    return this;
  }

  private ResponseEntity service() {
    return new ResponseEntity(
        HttpStatus.INTERNAL_SERVER_ERROR,
        MediaType.APPLICATION_JSON,
        JsonUtil.toJson(new ResponseMessageEntity<>(500, "SERVICE ERROR"))
            .getBytes(StandardCharsets.UTF_8));
  }

  @Override
  public boolean handleThrowable(HttpServerExchange httpServerExchange, ServletRequest servletRequest,
      ServletResponse servletResponse, Throwable throwable) {
    ResponseEntity service = service();
    byte[] body = service.body();
    try {
      httpServerExchange.setStatusCode(service.statusCode());
      servletResponse.setCharacterEncoding(StandardCharsets.UTF_8.name());
      servletResponse.setContentType(service.typeValue());
      servletResponse.setContentLength(body.length);
    } catch (RuntimeException ex) {
      logger.atWarn().message("Error response exception.").withCause(ex).log();
    }
    try (OutputStream os = servletResponse.getOutputStream()) {
      os.write(body);
      os.flush();
    } catch (IOException | RuntimeException ex) {
      logger.atError().message("Failed to return exception message.").withCause(ex).log();
      return false;
    }
    if (throwable != null) {
      logger.error("SERVICE ERROR.", throwable);
    } else {
      logger.error("UNKNOWN ERROR.");
    }
    return true;
  }
}
