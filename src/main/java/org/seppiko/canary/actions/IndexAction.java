/*
 * Copyright 2023 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.seppiko.canary.actions;

import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServletRequest;
import org.seppiko.canary.models.ResponseEntity;
import org.seppiko.commons.logging.Logging;
import org.seppiko.commons.logging.LoggingFactory;
import org.seppiko.commons.utils.StringUtil;

import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.util.LinkedHashMap;

/**
 * @author Leonard Woo
 */
public class IndexAction extends AbstractAction {

  private final Logging log = LoggingFactory.getLogging(this.getClass());

  @Override
  protected ResponseEntity contentHandleExecution(HttpServletRequest request)
          throws ServletException, IOException {
    String encoding = request.getCharacterEncoding();
    if (!StringUtil.hasText(encoding)) {
      encoding = StandardCharsets.UTF_8.name();
    }
    String body = new String(getBody(request.getInputStream(), request.getContentLength()), encoding);
    log.info("" + body);

    LinkedHashMap<String, String> headerMap = new LinkedHashMap<>();
    request.getHeaderNames().asIterator().forEachRemaining(name -> {
      StringBuilder values = new StringBuilder();
      request.getHeaders(name).asIterator().forEachRemaining(values::append);
      headerMap.put(name, values.toString());
    });
    log.info("" + headerMap);

    return failed("Bad message.");
  }
}
