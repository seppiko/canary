/*
 * Copyright 2023 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.seppiko.canary.utils;

import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Arrays;
import org.apache.commons.text.StringEscapeUtils;
import org.seppiko.canary.models.ResponseMediaEntity;
import org.seppiko.canary.models.ResponseMetricsEntity;
import org.seppiko.canary.models.ResponseStatusEntity;
import org.seppiko.canary.models.ResponseTimelineEntity;
import org.seppiko.canary.models.mastodon.Account;
import org.seppiko.canary.models.mastodon.Media;
import org.seppiko.canary.models.mastodon.TootLookup;
import org.seppiko.commons.utils.DatetimeUtil;
import org.seppiko.commons.utils.StringUtil;

/**
 * @author Leonard Woo
 */
public class MastodonUtil {

  private MastodonUtil() {}

  public static ResponseStatusEntity parserToot(TootLookup toot, boolean preview) {
    String tootText = toot.getContent();
    tootText = (tootText == null || tootText.isBlank()) ? "" : tootText;
    tootText = StringEscapeUtils.unescapeHtml4(tootText);
    //    tootText = StringUtil.unicodeEncode(tootText);
    tootText = tootText.trim();

    Account account = toot.getAccount();

    ArrayList<ResponseMediaEntity> medias = new ArrayList<>();
    if (toot.getMediaAttachments().length > 0) {
      medias.addAll(getMedias(toot.getMediaAttachments(),preview));
    }

    return new ResponseStatusEntity(
        toot.getId(),
        account.getUsername(),
        account.getDisplayName(),
        tootText,
        DatetimeUtil.parse("yyyy-MM-dd'T'HH:mm:ss.SSSX", toot.getCreatedAt())
            .format(DateTimeFormatter.RFC_1123_DATE_TIME),
        "",
        medias,
        convertMetrics(toot));
  }

  public static ResponseTimelineEntity parserTimeline(ArrayList<TootLookup> timeline, boolean preview) {
    ResponseTimelineEntity respTimeline = new ResponseTimelineEntity();
    if (timeline == null) {
      return respTimeline;
    }
    ArrayList<ResponseStatusEntity> statusList = new ArrayList<>();
    timeline.forEach(toot -> {
      if (toot != null) {
        statusList.add(parserToot(toot, preview));
      }
    });
    respTimeline.setStatuses(statusList);
    respTimeline.setCount(statusList.size());
    return respTimeline;
  }

  public static String tootMediaUrlReplace(String type, String url) {
    return url;
  }

  private static ArrayList<ResponseMediaEntity> getMedias(Media[] medias, boolean needPreview) {
    ArrayList<ResponseMediaEntity> mediaList = new ArrayList<>();
    if (medias == null || medias.length < 1) {
      return null;
    }

    Arrays.stream(medias).forEach(media -> {
      mediaList.add(new ResponseMediaEntity(media.getId(), media.getType(), 0L, media.getUrl()));
      if (needPreview) {
        mediaList.add(new ResponseMediaEntity(media.getId(), "preview", 0L, media.getPreviewUrl()));
      }
    });

    return mediaList;
  }

  private static ResponseMetricsEntity convertMetrics (TootLookup toot) {
    ResponseMetricsEntity metrics = new ResponseMetricsEntity();

    metrics.setStatusMetrics(toot.getReblogsCount(), toot.getRepliesCount(), toot.getFavouritesCount(), 0);

    Account account = toot.getAccount();
    metrics.setUserMetrics(account.getFollowersCount(), account.getFollowingCount(),
        account.getStatusesCount(), 0, 0);

    return metrics;
  }

  public static int accountStatusesIdCheck(String maxId, String sinceId) {
    if (StringUtil.hasText(maxId)
        && StringUtil.hasText(sinceId)
        && StringUtil.isDigit(maxId)
        && StringUtil.isDigit(sinceId)) {
      long until = Long.parseLong(maxId);
      long since = Long.parseLong(sinceId);
      return (until > since) ? 1 : 0;
    }
    return -1;
  }
}
