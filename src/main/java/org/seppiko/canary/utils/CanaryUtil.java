/*
 * Copyright 2023 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.seppiko.canary.utils;

import java.io.PrintStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Objects;
import java.util.Optional;
import java.util.function.Function;
import java.util.regex.Matcher;
import org.seppiko.commons.logging.Logging;
import org.seppiko.commons.logging.LoggingFactory;
import org.seppiko.commons.utils.CharUtil;
import org.seppiko.commons.utils.CollectionUtil;
import org.seppiko.commons.utils.Environment;
import org.seppiko.commons.utils.StringUtil;

/**
 * @author Leonard Woo
 */
public class CanaryUtil {

  private static final Logging log = LoggingFactory.getLogging(CanaryUtil.class);

  private CanaryUtil() {}

  public static boolean search(String input, String regex) {
    try {
      return Objects.requireNonNull(StringUtil.getMatcher(regex, input)).find();
    } catch (NullPointerException ignored) {
    }
    return false;
  }

  public static String find(String regex, String input) {
    Matcher m = StringUtil.getMatcher(regex, input);
    try {
      Objects.requireNonNull(m);
      if (m.find()) {
        return m.group(0);
      }
    } catch (RuntimeException ignored) {
    }
    return "";
  }

  public static String[] regexArray(String input, String regex) {
    return input.contains(regex) ? input.split(regex) : new String[] {input, input};
  }

  public static <T> T getOrElse(T value, T other) {
    return Optional.ofNullable(value).orElse(other);
  }

  public static <T> ArrayList<T> distinct(
      ArrayList<T> originalList, ArrayList<T> newList, Function<? super T, ? extends String> func) {
    HashMap<String, T> objMap = new HashMap<>();
    if (originalList != null) {
      objMap.putAll(CollectionUtil.populateMap(originalList, func));
    }
    if (newList != null) {
      objMap.putAll(CollectionUtil.populateMap(newList, func));
    }
    return new ArrayList<>(objMap.values());
  }

  private static final String[] BANNER = {
      "",
      " _____                  _ _           _____                              ",
      "/  ___|                (_) |         /  __ \\                             ",
      "\\ `--.  ___ _ __  _ __  _| | _____   | /  \\/ __ _ _ __   __ _ _ __ _   _ ",
      " `--. \\/ _ \\ '_ \\| '_ \\| | |/ / _ \\  | |    / _` | '_ \\ / _` | '__| | | |",
      "/\\__/ /  __/ |_) | |_) | |   < (_) | | \\__/\\ (_| | | | | (_| | |  | |_| |",
      "\\____/ \\___| .__/| .__/|_|_|\\_\\___/   \\____/\\__,_|_| |_|\\__,_|_|   \\__, |",
      "           | |   | |                                                __/ |",
      "           |_|   |_|                                               |___/ "
  };
  private static final String PROJECT_NAME = ":: Seppiko Canary ::";

  public static void printBanner(PrintStream out) {
    try {
      String version = VersionFactory.VERSION;
      version = (version != null)? ("(v" + version + ")"): "";
      int maxLength = 0;
      for (String line : BANNER) {
        out.println(line);
        maxLength = Math.max(maxLength, line.length());
      }

      StringBuilder padding = new StringBuilder();
      while (padding.length() < (maxLength - (version.length() + PROJECT_NAME.length()) )) {
        padding.append(CharUtil.SPACE);
      }
      out.println(toString(PROJECT_NAME, padding.toString(), version));
      out.println();
      out.flush();
    } catch (Exception ex) {
      log.warn("BANNER load failed.", ex);
    }
  }

  private static String toString(String... strs) {
    StringBuilder sb = new StringBuilder();
    for (String str : strs) {
      sb.append(str);
    }
    return sb.toString();
  }

  public static String getCurrentEnv() {
    return String.format("Starting using Java %s on %s (%s) with PID %d (started by %s in %s)",
        System.getProperty("java.version"),
        System.getenv("COMPUTERNAME"),
        System.getProperty("os.name"),
        Environment.CURRENT_PROCESS.pid(),
        System.getenv("USERNAME"),
        System.getProperty("user.dir"));
  }

  public static String getCurrentBuildTimestamp() {
    return String.format("Current build timestamp: %s",
            VersionFactory.BUILD_TIMESTAMP);
  }
}
