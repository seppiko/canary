/*
 * Copyright 2023 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.seppiko.canary.utils;

import org.seppiko.canary.configures.APIEntity;
import org.seppiko.canary.configures.CanaryConfiguration;
import org.seppiko.commons.utils.StringUtil;

/**
 * @author Leonard Woo
 */
public class MastodonUrlUtil {

  private static final APIEntity config = CanaryConfiguration.getInstance().getMastodon();

  public static final String MASTODON_DOMAIN;

  public static final String MASTODON_SINGLE_STATUS = "/api/v1/statuses/%s";

  public static final String MASTODON_ACCOUNT = "/api/v1/accounts/%s";

  public static final String MASTODON_SEARCH = "/api/v2/search";

  public static final String MASTODON_TIMELINE = "/api/v1/accounts/%s/statuses";

  static {
    if (config != null) {
      MASTODON_DOMAIN = deleteEndsWith(config.getServer(), "/");
    } else {
      MASTODON_DOMAIN = "";
    }
  }

  private static String deleteEndsWith(String input, String suffix) {
    return input.endsWith(suffix)? input.substring(0, (input.length() - suffix.length())): input;
  }

  public static boolean isToot(String uri) {
    return StringUtil.matches("^"+ MASTODON_DOMAIN + "/\\@[\\w\\-]+/\\d+$", uri);
  }

  public static String splitTootId(String uri) {
    return CanaryUtil.find("/\\d+", uri).substring(1);
  }

  public static String splitMastodonUsername(String uri) {
    String _usr = CanaryUtil.find("/@[\\w\\-]+/", uri);
    return _usr.substring(1, _usr.length() - 1);
  }
}
