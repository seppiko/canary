/*
 * Copyright 2023 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.seppiko.canary.utils;

import org.seppiko.commons.logging.Logging;
import org.seppiko.commons.logging.LoggingFactory;
import org.seppiko.commons.utils.StringUtil;

/**
 * Twitter URL Util
 *
 * @author Leonard Woo
 */
public class TwitterUrlUtil {

  private TwitterUrlUtil() {}

  private static final Logging log = LoggingFactory.getLogging(TwitterUrlUtil.class);

  public static final String TWITTER_API = "https://api.twitter.com/2";
  public static final String TWITTER_API2_LOOKUP = "/users/by/username/%s";
  public static final String TWITTER_API2_TIMELINE = "/users/%s/tweets";
  public static final String TWITTER_API2_TIMELINE_OPTIONS = "exclude=retweets,replies";
  public static final String TWITTER_API2_TWEET = "/tweets/%s";
  public static final String TWITTER_API2_TWEETS = "/tweets";

  public static final String TWITTER_API2_PROBLEMS = "/problems";

  public static final String TWITTER_API2_EXPANSIONS =
          "expansions=attachments.media_keys,author_id,entities.mentions.username,geo.place_id," +
                  "in_reply_to_user_id,referenced_tweets.id,referenced_tweets.id.author_id";

  public static final String TWITTER_API2_TWEET_FIELDS =
          "tweet.fields=attachments,author_id,context_annotations,conversation_id,created_at,entities,geo,id," +
                  "in_reply_to_user_id,lang,public_metrics,possibly_sensitive,referenced_tweets,source," +
                  "reply_settings,text,withheld";

  public static final String TWITTER_API2_MEDIA_FIELDS =
          "media.fields=duration_ms,height,media_key,preview_image_url,type,url," +
                  "width,public_metrics,alt_text,variants";

  public static final String TWITTER_API2_USER_FIELDS =
          "user.fields=created_at,description,entities,id,location,name,pinned_tweet_id," +
                  "profile_image_url,protected,public_metrics,url,username," +
                  "verified,withheld";

  public static final String TWITTER_API2_PLACE_FIELDS =
          "place.fields=contained_within,country,country_code,full_name,geo,id,name,place_type";

  public static final String TWITTER_API2_ALL_FIELDS;

  static {
    TWITTER_API2_ALL_FIELDS = TWITTER_API2_EXPANSIONS +
            "&" + TWITTER_API2_MEDIA_FIELDS +
            "&" + TWITTER_API2_TWEET_FIELDS +
            "&" + TWITTER_API2_USER_FIELDS +
            "&" + TWITTER_API2_PLACE_FIELDS;

  }

  public static boolean isTweetUrl(String url) {
    return StringUtil.matches("^http[s]?://twitter.com/[\\w\\.-]+/status/\\d+.*", url);
  }

  public static boolean isTwitterUser(String url) {
    return StringUtil.matches("^http[s]?://twitter.com/[\\w\\.\\-]+(^(/status)).*", url);
  }

  public static boolean isTweetPhoto(String url) {
    return StringUtil.matches("^http[s]?://pbs.twimg.com/media/[\\w\\-]*\\.\\w{3,4}$", url);
  }

  public static boolean isTweetVideo(String url) {
    return StringUtil.matches("^http[s]?://video.twimg.com/ext_tw_video/\\d+/pu/vid/\\d+x\\d+/[\\w\\-]*.\\w{3,4}.*$", url);
  }

  public static boolean isTweetVideoThumb(String url) {
    return StringUtil.matches("^http[s]?://pbs.twimg.com/ext_tw_video_thumb/\\d*/pu/img/[\\w\\-]*\\.\\w{3,4}$", url);
  }

  public static String splitTweetId(String uri) {
    String tweetStatus = "status/";
    return CanaryUtil.find(tweetStatus + "\\d+", uri).substring(tweetStatus.length());
  }

  public static String splitTwitterUsername(String uri) {
    String twitterUser = ".com/";
    return CanaryUtil.find(twitterUser + "[\\w\\.-]+", uri).substring(twitterUser.length());
  }

  public static String splitTwitterProblem(final String type) {
    String type1 = CanaryUtil.find(TWITTER_API2_PROBLEMS + "/[\\w\\.\\-]+", type);
    if ("".equals(type1)) {
      return type;
    }
    return type1.substring(TWITTER_API2_PROBLEMS.length() + 1);
  }
}
