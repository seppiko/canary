/*
 * Copyright 2023 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.seppiko.canary.utils;

import org.seppiko.canary.configures.CanaryConfiguration;
import org.seppiko.canary.configures.DownloadEntity;
import org.seppiko.canary.configures.ProxyEntity;
import org.seppiko.canary.configures.APIEntity;
import org.seppiko.commons.logging.Logging;
import org.seppiko.commons.logging.LoggingFactory;
import org.seppiko.commons.utils.Environment;
import org.seppiko.commons.utils.http.*;

import java.net.InetSocketAddress;
import java.net.URISyntaxException;
import java.net.http.HttpRequest;
import java.net.http.HttpResponse;
import java.util.NoSuchElementException;
import java.util.Objects;
import java.util.concurrent.TimeUnit;

/**
 * Http client util
 *
 * @author Leonard Woo
 */
public class HttpUtil {

  private static final Logging log = LoggingFactory.getLogging(HttpUtil.class);

  private static final CanaryConfiguration CONFIG = CanaryConfiguration.getInstance();
  private static final ProxyEntity PROXY = CONFIG.getProxy();
  private static final DownloadEntity DOWNLOAD = CONFIG.getDownload();
  private static final HttpHeaders REQ_HEADERS;
  private static final HttpHeaders DL_HEADERS;
  private static InetSocketAddress proxyAddr = null;

  static {
    REQ_HEADERS = HttpHeaders.newHeaders();
    REQ_HEADERS.addHeaders("Accept", MediaType.APPLICATION_JSON.getValue());
    REQ_HEADERS.addHeaders("Accept-Language", "en-US,en;q=0.5");
    REQ_HEADERS.addHeaders("Content-Type", MediaType.APPLICATION_JSON.getValue());
    REQ_HEADERS.addHeaders("Cache-Control", "no-cache");
    REQ_HEADERS.addHeaders("Pragma", "no-cache");
    REQ_HEADERS.addHeaders("User-Agent", CanaryConfiguration.UA);

    DL_HEADERS = HttpHeaders.newHeaders();
    DL_HEADERS.addHeaders("Accept", MediaType.ALL.getValue());
    DL_HEADERS.addHeaders("Cache-Control", "no-cache");
    DL_HEADERS.addHeaders("Pragma", "no-cache");
    DL_HEADERS.addHeaders("User-Agent", CanaryConfiguration.UA);

    if (PROXY != null && PROXY.isEnable()) {
      proxyAddr = new InetSocketAddress(PROXY.getHost(), PROXY.getPort());
    }
  }

  public static HttpResponse<String> get(final String url, final APIEntity apiConfig) {
    HttpResponse<String> resp = null;
    int i = 0;
    do {
      log.debug("try request " + i + " >>> " + url);
      try {
        resp = get0(url, apiConfig.getTimeout(), apiConfig.getToken());
        if (resp != null) {
          break;
        }
        TimeUnit.SECONDS.sleep(apiConfig.getRetryIntervalSeconds());
      } catch (Exception ex) {
        if (log.isDebugEnable()) {
          log.warn("Try " + i + " Exception " + ex.getMessage());
        }
      }
      i++;
    } while (i < apiConfig.getNumberOfRetries());

    if (null == resp) {
      log.warn("Try request " + url + " unexpectedly closed the connection.");
    }

    return resp;
  }

  private static HttpResponse<String> get0(final String url, final int timeout, final String token) throws HttpClientException {
    try {
      REQ_HEADERS.setHeaders("Authorization", "Bearer " + token);
      HttpRequest request = HttpClientUtil.getRequest(url, HttpMethod.GET, timeout, REQ_HEADERS, (String) null);
      return HttpClientUtil.getResponseString(request, TLSUtil.NULL_SSL_CONTEXT, proxyAddr);
    } catch (URISyntaxException | RuntimeException | HttpClientException ex) {
      throw new HttpClientException("Request " + url + " unexpectedly closed the connection.", ex);
    }
  }

  private static String contentType = MediaType.ALL.getValue();

  public static String getContentType() {
    return contentType;
  }

  public static byte[] download(final String url) {
    byte[] data = null;
    int i = 0;
    do {
      log.debug("try download " + i + " >>> " + url);
      try {
        data = download0(url);
        if (data != null && data.length > 0) {
          break;
        }
        TimeUnit.SECONDS.sleep(DOWNLOAD.getRetryIntervalSeconds());
      } catch (Exception ex) {
        if (log.isDebugEnable()) {
          log.warn("" + ex.getMessage());
        }
      }
      i++;
    } while (i < DOWNLOAD.getNumberOfRetries());
    return data;
  }

  private static byte[] download0(final String url) {
    try {
      var req = HttpClientUtil.getRequest(url, HttpMethod.GET, DOWNLOAD.getTimeout(),
              DL_HEADERS, "");
      var resp = AsyncHttpClientUtil.getAsyncResponseByteArray(req, TLSUtil.NULL_SSL_CONTEXT, proxyAddr);
      Objects.requireNonNull(resp);
      try {
        contentType = resp.headers().firstValue("Content-Type").orElseThrow();
      } catch (NullPointerException | NoSuchElementException ignored) {
      }
      return resp.body();
    } catch (URISyntaxException | HttpClientException | RuntimeException ex) {
      if (log.isDebugEnable()) {
        log.error("HTTP request failed " + ex.getMessage());
      }
      return null;
    } catch (Throwable t) {
      log.error("Unknown error: " + t.getMessage());
    }
    return Environment.EMPTY_BYTE_ARRAY;
  }

}
