/*
 * Copyright 2023 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.seppiko.canary.utils;

import io.minio.*;
import io.minio.errors.*;
import org.seppiko.commons.logging.Logging;
import org.seppiko.commons.logging.LoggingFactory;
import org.seppiko.commons.utils.StringUtil;

import java.io.IOException;
import java.io.InputStream;
import java.security.GeneralSecurityException;

/**
 * @author Leonard Woo
 */
public class S3Util {

  private static final Logging logger = LoggingFactory.getLogging(S3Util.class);

  private MinioClient minioClient;

  private final String region = "us-east-1";

  private String bucketName;

  public S3Util(String url, String accessKey, String secretKey, String region) {
    if (region == null || StringUtil.nonText(region)) {
      region = this.region;
    }
    try {
      minioClient = MinioClient.builder()
              .endpoint(url)
              .credentials(accessKey, secretKey)
              .region(region)
              .build();
    } catch (RuntimeException ex) {
      logger.error("" + ex.getMessage());
    }
  }

  public void createBucket(String bucketName)
          throws MinioException, IOException, GeneralSecurityException {
    boolean found = minioClient.bucketExists(BucketExistsArgs.builder().bucket(bucketName).build());
    if (!found) {
      minioClient.makeBucket(MakeBucketArgs.builder().bucket(bucketName).build());
    }
    this.bucketName = bucketName;
  }

  public boolean existObjectName(String objectName) {
    boolean exist = false;
    try {
      minioClient.statObject(StatObjectArgs.builder()
              .bucket(bucketName)
              .object(objectName).build());
      exist = true;
    } catch (ErrorResponseException e) {
      logger.info(e.getMessage());
    } catch (MinioException | RuntimeException | GeneralSecurityException | IOException ex) {
      logger.error(ex.getMessage());
    }
    return exist;
  }

  public void uploadStream(String objectName, InputStream is, int size)
          throws MinioException, IOException, GeneralSecurityException {
    minioClient.putObject(PutObjectArgs.builder()
            .bucket(bucketName)
            .object(objectName)
            .stream(is, size, -1)
            .build());
  }

}
