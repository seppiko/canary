/*
 * Copyright 2023 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.seppiko.canary.utils;

import org.seppiko.commons.logging.Logging;
import org.seppiko.commons.logging.LoggingFactory;
import org.seppiko.commons.utils.StreamUtil;
import org.seppiko.commons.utils.http.MediaType;
import org.seppiko.commons.utils.http.URIUtil;

import java.io.File;
import java.io.IOException;
import java.nio.file.FileAlreadyExistsException;

/**
 * @author Leonard Woo
 */
public class FileUtil {

  private static final Logging log = LoggingFactory.getLogging(HttpUtil.class);

  public static String getFilename(String url) {
    try {
      return URIUtil.getLastPathname(url);
    } catch (IllegalArgumentException | NullPointerException ex) {
      log.warn("" + ex.getMessage());
    }
    return url;
  }

  public static String getFileExt(String mediaType) {
    switch (MediaType.findByValue(mediaType)) {
      case IMAGE_JPEG -> {
        return ".jpg";
      }
      case IMAGE_PNG -> {
        return ".png";
      }
      case VIDEO_MP4 -> {
        return ".mp4";
      }
    }
    log.warn("Not found media type: " + mediaType);
    return "";
  }

  public static boolean includeExt(String filename) {
    try {
      return URIUtil.getFileExtension(filename) != null;
    } catch (NullPointerException ignored) {
      log.warn("Filename must be NOT NULL");
    }
    return false;
  }

  public static boolean saveFile(byte[] data, String pathname) {
    try {
      saveFile0(data, pathname);
    } catch (FileAlreadyExistsException ex) {
      log.warn(ex.getMessage());
    } catch (IOException | IllegalArgumentException | NullPointerException ex) {
      log.error("File save failed " + ex.getMessage());
      return false;
    }
    return true;
  }

  private static void saveFile0(byte[] data, String pathname)
          throws IOException, IllegalArgumentException, NullPointerException, FileAlreadyExistsException {
    File file = new File(pathname);
    if (file.exists()) {
      throw new FileAlreadyExistsException("File " + pathname + " already exists.");
    }
    StreamUtil.writeFile(data, file);
  }

}
