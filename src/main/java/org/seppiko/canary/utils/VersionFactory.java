/*
 * Copyright 2023 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.seppiko.canary.utils;

import org.seppiko.commons.logging.Logging;
import org.seppiko.commons.logging.LoggingFactory;

import java.io.InputStream;
import java.util.jar.Attributes;
import java.util.jar.Manifest;

/**
 * Version Factory
 *
 * @author Leonard Woo
 */
public class VersionFactory {

  private static final Logging log = LoggingFactory.getLogging(VersionFactory.class);

  public static String GROUP_ID;
  public static String ARTIFACT_ID;
  public static String VERSION;
  public static String BUILD_TIMESTAMP;

  static {
    readManifest();
  }

  private VersionFactory() {}

  private static void readManifest() {
    try (InputStream mis = VersionFactory.class.getClassLoader().getResourceAsStream("META-INF/MANIFEST.MF")) {
      Manifest m = new Manifest(mis);
      Attributes mainAttrs = m.getMainAttributes();
      GROUP_ID = mainAttrs.getValue("Implementation-Vendor");
      ARTIFACT_ID = mainAttrs.getValue("Implementation-Title");
      VERSION = mainAttrs.getValue("Implementation-Version");
      BUILD_TIMESTAMP = mainAttrs.getValue("Build-Timestamp");
    } catch (Throwable t) {
      log.error("File `MANIFEST.MF` load failed.", t);

      Package pkg = VersionFactory.class.getPackage();
      GROUP_ID = pkg.getImplementationVendor();
      ARTIFACT_ID = pkg.getImplementationTitle();
      VERSION = pkg.getImplementationVersion();
      BUILD_TIMESTAMP = "";
    }
  }
}
