/*
 * Copyright 2023 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.seppiko.canary.utils;

import java.lang.management.ClassLoadingMXBean;
import java.lang.management.ManagementFactory;
import java.lang.management.RuntimeMXBean;
import java.lang.management.ThreadInfo;
import java.lang.management.ThreadMXBean;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.seppiko.canary.models.metrics.ClazzLoadingMXBean;
import org.seppiko.commons.logging.Logging;
import org.seppiko.commons.logging.LoggingFactory;

/**
 * @author Leonard Woo
 */
public class JmxMetricsUtil {

  private static final Logging log = LoggingFactory.getLogging(JmxMetricsUtil.class);

  public static org.seppiko.canary.models.metrics.ThreadMXBean getThread() {
    org.seppiko.canary.models.metrics.ThreadMXBean mxBean = new org.seppiko.canary.models.metrics.ThreadMXBean();
    Thread thread = new Thread();
    mxBean.setThreadCount(thread.getThreadCount());
    mxBean.setPeakThreadCount(thread.getPeakThreadCount());
    mxBean.setTotalStartedThreadCount(thread.getTotalStartedThreadCount());
    mxBean.setDaemonThreadCount(thread.getDaemonThreadCount());
    mxBean.setAllThreadInfo(thread.getAllThreadInfo());
    mxBean.setAllDeadlockedThreadInfo(thread.getAllDeadlockedThreadInfo());
    return mxBean;
  }

  public static ClazzLoadingMXBean getClazzLoading() {
    ClazzLoadingMXBean mxBean = new ClazzLoadingMXBean();
    ClazzLoading clazzLoading = new ClazzLoading();
    mxBean.setTotalLoadedCount(clazzLoading.getTotalLoadedClassCount());
    mxBean.setLoadedCount(clazzLoading.getLoadedClassCount());
    mxBean.setUnloadedCount(clazzLoading.getUnloadedClassCount());
    return mxBean;
  }

  public static org.seppiko.canary.models.metrics.RuntimeMXBean getRuntime() throws SecurityException {
    org.seppiko.canary.models.metrics.RuntimeMXBean mxBean = new org.seppiko.canary.models.metrics.RuntimeMXBean();
    Runtime runtime = new Runtime();
    mxBean.setName(runtime.getName());
    mxBean.setPid(runtime.getPid());
    mxBean.setBootClassPath(runtime.getBootClassPath());
    mxBean.setLibraryPath(runtime.getLibraryPath());
    mxBean.setClazzPath(runtime.getClassPath());
    mxBean.setSpecName(runtime.getSpecName());
    mxBean.setSpecVendor(runtime.getSpecVendor());
    mxBean.setSpecVersion(runtime.getSpecVersion());
    mxBean.setVmName(runtime.getVmName());
    mxBean.setVmVendor(runtime.getVmVendor());
    mxBean.setVmVersion(runtime.getVmVersion());
    mxBean.setManagementSpecVersion(runtime.getManagementSpecVersion());
    mxBean.setStartTime(runtime.getStartTime());
    mxBean.setUptime(runtime.getUptime());
    mxBean.setSystemProperties(runtime.getSystemProperties());
    mxBean.setInputArguments(runtime.getInputArguments());
    return mxBean;
  }

  public static class Thread {

    private final ThreadMXBean thread = ManagementFactory.getThreadMXBean();

    public int getThreadCount() {
      return thread.getThreadCount();
    }

    public int getPeakThreadCount() {
      return thread.getPeakThreadCount();
    }

    public long getTotalStartedThreadCount() {
      return thread.getTotalStartedThreadCount();
    }

    public long getDaemonThreadCount() {
      return thread.getDaemonThreadCount();
    }

    public Map<Long, ThreadInfo> getAllThreadInfo() {
      Map<Long, ThreadInfo> allThreadInfo = new HashMap<>();
      try {
        long[] threadIds = thread.getAllThreadIds();
        for (long threadId : threadIds) {
          ThreadInfo threadInfo = thread.getThreadInfo(threadId);
          allThreadInfo.put(threadId, threadInfo);
        }
      } catch (UnsupportedOperationException | IllegalArgumentException ex) {
        log.warn(ex.getMessage());
      } catch (NullPointerException ex) {
        return null;
      }
      return allThreadInfo;
    }

    public Map<Long, ThreadInfo> getAllDeadlockedThreadInfo() {
      Map<Long, ThreadInfo> allDeadlockedThreadInfo = new HashMap<>();
      try {
        long[] threadIds = thread.findDeadlockedThreads();
        for (long threadId : threadIds) {
          ThreadInfo threadInfo = thread.getThreadInfo(threadId);
          allDeadlockedThreadInfo.put(threadId, threadInfo);
        }
      } catch (UnsupportedOperationException | IllegalArgumentException ex) {
        log.warn(ex.getMessage());
      } catch (NullPointerException ex) {
        return null;
      }
      return allDeadlockedThreadInfo;
    }

  }

  public static class ClazzLoading {

    private final ClassLoadingMXBean classLoading = ManagementFactory.getClassLoadingMXBean();

    public long getTotalLoadedClassCount() {
      return classLoading.getTotalLoadedClassCount();
    }

    public int getLoadedClassCount() {
      return classLoading.getLoadedClassCount();
    }

    public long getUnloadedClassCount() {
      return classLoading.getUnloadedClassCount();
    }

  }

  public static class Runtime {

    private final RuntimeMXBean runtime = ManagementFactory.getRuntimeMXBean();

    public String getName() {
      return runtime.getName();
    }

    public String getPid() {
      return getName().split("@")[0];
    }

    public String getBootClassPath() {
      try {
        return runtime.getBootClassPath();
      } catch (UnsupportedOperationException ex) {
        log.warn(ex.getMessage());
      }
      return null;
    }

    public String getLibraryPath() {
      return runtime.getLibraryPath();
    }

    public String getClassPath() {
      return runtime.getClassPath();
    }

    public String getSpecName() {
      return runtime.getSpecName();
    }

    public String getSpecVendor() {
      return runtime.getSpecVendor();
    }

    public String getSpecVersion() {
      return runtime.getSpecVersion();
    }

    public String getVmName() {
      return runtime.getVmName();
    }

    public String getVmVendor() {
      return runtime.getVmVendor();
    }

    public String getVmVersion() {
      return runtime.getVmVersion();
    }

    public String getManagementSpecVersion() {
      return runtime.getManagementSpecVersion();
    }

    public long getStartTime() {
      return runtime.getStartTime();
    }

    public long getUptime() {
      return runtime.getUptime();
    }

    public Map<String, String> getSystemProperties() {
      return runtime.getSystemProperties();
    }

    public List<String> getInputArguments() {
      return runtime.getInputArguments();
    }
  }
}
