/*
 * Copyright 2023 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.seppiko.canary.utils;

import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Objects;
import java.util.concurrent.atomic.AtomicReference;
import org.apache.commons.text.StringEscapeUtils;
import org.seppiko.canary.models.ResponseGeoEntity;
import org.seppiko.canary.models.ResponseMediaEntity;
import org.seppiko.canary.models.ResponseMetricsEntity;
import org.seppiko.canary.models.ResponseTimelineEntity;
import org.seppiko.canary.models.ResponseStatusEntity;
import org.seppiko.canary.models.twitterv2.Attachments;
import org.seppiko.canary.models.twitterv2.Data;
import org.seppiko.canary.models.twitterv2.Geo;
import org.seppiko.canary.models.twitterv2.Includes;
import org.seppiko.canary.models.twitterv2.Media;
import org.seppiko.canary.models.twitterv2.Meta;
import org.seppiko.canary.models.twitterv2.Place;
import org.seppiko.canary.models.twitterv2.PublicMetrics;
import org.seppiko.canary.models.twitterv2.Timeline;
import org.seppiko.canary.models.twitterv2.TweetLookup;
import org.seppiko.canary.models.twitterv2.TweetsLookup;
import org.seppiko.canary.models.twitterv2.Url;
import org.seppiko.canary.models.twitterv2.User;
import org.seppiko.canary.models.twitterv2.Variant;
import org.seppiko.commons.logging.Logging;
import org.seppiko.commons.logging.LoggingFactory;
import org.seppiko.commons.utils.CollectionUtil;
import org.seppiko.commons.utils.DatetimeUtil;
import org.seppiko.commons.utils.StringUtil;

/**
 * Twitter API v2 Util
 *
 * @author Leonard Woo
 */
public class Twitter2Util {

  private static final Logging log = LoggingFactory.getLogging(Twitter2Util.class);

  public static ResponseStatusEntity parserTweetV2(
      TweetLookup tweet, boolean maxVideo, boolean needPreview) {
    Data data = tweet.getData();
    Includes includes = tweet.getIncludes();

    String tweetText = data.getText();
    tweetText = (tweetText == null || tweetText.isBlank()) ? "" : tweetText;
    tweetText = StringEscapeUtils.unescapeHtml4(tweetText);
    //    tweetText = StringUtil.unicodeEncode(tweetText);
    tweetText =
        replaceTweetTextShortUrl(
            tweetText, (data.getEntities() == null ? null : data.getEntities().getUrls()));
    tweetText = tweetText.trim();

    if (!StringUtil.isNullOrEmpty(data.getInReplyToUserId())) {
      tweetText = "Replying to " + tweetText;
    }

    User user = includes.getUsers().get(0);

    ArrayList<ResponseMediaEntity> medias = new ArrayList<>();
    if (data.getAttachments() != null) {
      medias = getMediaList(includes.getMedia(), maxVideo, needPreview);
    }

    Geo geo = data.getGeo();
    String placeFullname = "";
    if (geo != null) {
      placeFullname = getPlaceFullname(includes.getPlaces().get(0));
    }

    return new ResponseStatusEntity(
        data.getId(),
        user.getUsername(),
        user.getName(),
        tweetText,
        DatetimeUtil.parse("yyyy-MM-dd'T'HH:mm:ss.SSSX", data.getCreatedAt())
            .format(DateTimeFormatter.RFC_1123_DATE_TIME),
        placeFullname,
        medias,
        convertMetrics(data.getPublicMetrics(), user.getPublicMetrics()));
  }

  public static ArrayList<ResponseStatusEntity> parserTweetsV2(
      TweetsLookup tweets, boolean maxVideo, boolean preview) {
    Objects.requireNonNull(tweets, "Tweets is null");
    ArrayList<ResponseStatusEntity> tweetList = new ArrayList<>();

    ArrayList<Data> dataList = tweets.getData();
    Includes includes = tweets.getIncludes();

    if (dataList == null || dataList.size() < 1) {
      return tweetList;
    }

    HashMap<String, Media> mediaMap = null;
    HashMap<String, User> userMap = null;
    HashMap<String, Place> placeMap = null;
    if (includes != null) {
      mediaMap = convertMedia(includes.getMedia());
      userMap = convertUser(includes.getUsers());
      placeMap = convertPlace(includes.getPlaces());
    }

    for (Data data : dataList) {
      if (data == null) {
        continue;
      }

      String tweetText = data.getText();
      tweetText = (tweetText == null || tweetText.isBlank()) ? "" : tweetText;
      tweetText = StringEscapeUtils.unescapeHtml4(tweetText);
      //      tweetText = StringUtil.unicodeEncode(tweetText);
      tweetText =
          replaceTweetTextShortUrl(
              tweetText, (data.getEntities() == null ? null : data.getEntities().getUrls()));
      tweetText = tweetText.trim();

      if (!StringUtil.isNullOrEmpty(data.getInReplyToUserId())) {
        tweetText = "Replying to " + tweetText;
      }

      User user = new User();
      if (userMap != null && data.getAuthorId() != null) {
        user = userMap.get(data.getAuthorId());
      }

      Attachments attachments = data.getAttachments();
      ArrayList<ResponseMediaEntity> medias = new ArrayList<>();
      if (attachments != null && mediaMap != null) {
        medias = getMediaList(attachments.getMediaKeys(), mediaMap, maxVideo, preview);
      }

      Geo geo = data.getGeo();
      String placeFullname = null;
      if (geo != null && placeMap != null) {
        placeFullname = getPlaceFullname(placeMap.get(geo.getPlaceId()));
      }

      ResponseStatusEntity respTweet =
          new ResponseStatusEntity(
              data.getId(),
              user.getUsername(),
              user.getName(),
              tweetText,
              DatetimeUtil.parse("yyyy-MM-dd'T'HH:mm:ss.SSSX", data.getCreatedAt())
                  .format(DateTimeFormatter.RFC_1123_DATE_TIME),
              placeFullname,
              medias,
              convertMetrics(data.getPublicMetrics(), user.getPublicMetrics()));

      tweetList.add(respTweet);
    }

    return tweetList;
  }

  private static String replaceTweetTextShortUrl(String tweetText, ArrayList<Url> urls) {
    if (urls == null || urls.isEmpty()) {
      return tweetText;
    }
    AtomicReference<String> text = new AtomicReference<>(tweetText);
    urls.forEach(
        url -> {
          if (url.getExpandedUrl().startsWith("https://twitter.com/")) {
            text.set(text.get().replace(url.getUrl(), ""));
          } else {
            text.set(text.get().replace(url.getUrl(), url.getExpandedUrl()));
          }
        });
    return text.get();
  }

  public static ResponseTimelineEntity parserTimelineV2(
      Timeline timeline, boolean maxVideo, boolean preview) {
    ResponseTimelineEntity respTimeline = new ResponseTimelineEntity();
    if (timeline == null) {
      return respTimeline;
    }

    try {
      TweetsLookup tweetsLookup = new TweetsLookup();
      tweetsLookup.setData(timeline.getData());

      Includes oldIncludes = timeline.getIncludes();
      Includes targetIncludes = new Includes();
      if (oldIncludes != null) {
        targetIncludes.setMedia(oldIncludes.getMedia());
        targetIncludes.setUsers(oldIncludes.getUsers());
        targetIncludes.setPlaces(oldIncludes.getPlaces());
      }
      tweetsLookup.setIncludes(targetIncludes);

      respTimeline.setStatuses(parserTweetsV2(tweetsLookup, maxVideo, preview));

      Meta meta = timeline.getMeta();
      respTimeline.setUntilId(meta.getOldestId());
      respTimeline.setSinceId(meta.getNewestId());
      respTimeline.setCount(meta.getResultCount());
    } catch (RuntimeException ex) {
      log.warn("Something is " + ex.getMessage());
      if (log.isDebugEnable()) {
        log.warn("cause ", ex);
      }
    }
    return respTimeline;
  }

  public static int timelineIdCheck(String untilId, String sinceId) {
    if (StringUtil.hasText(untilId)
        && StringUtil.hasText(sinceId)
        && StringUtil.isDigit(untilId)
        && StringUtil.isDigit(sinceId)) {
      long until = Long.parseLong(untilId);
      long since = Long.parseLong(sinceId);
      return (until > since) ? 1 : 0;
    }
    return -1;
  }

  public static Timeline mergeTimeline(Timeline oldTimeline, Timeline newTimeline) {
    Timeline tl = new Timeline();

    tl.setData(CanaryUtil.distinct(oldTimeline.getData(), newTimeline.getData(), Data::getId));

    Includes oldIncludes = CanaryUtil.getOrElse(oldTimeline.getIncludes(), new Includes());
    Includes newIncludes = CanaryUtil.getOrElse(newTimeline.getIncludes(), new Includes());
    Includes targetIncludes = new Includes();
    targetIncludes.setMedia(
        CanaryUtil.distinct(oldIncludes.getMedia(), newIncludes.getMedia(), Media::getMediaKey));
    targetIncludes.setUsers(
        CanaryUtil.distinct(oldIncludes.getUsers(), newIncludes.getUsers(), User::getId));
    targetIncludes.setPlaces(
        CanaryUtil.distinct(oldIncludes.getPlaces(), newIncludes.getPlaces(), Place::getId));
    tl.setIncludes(targetIncludes);

    Meta oldMeta = CanaryUtil.getOrElse(oldTimeline.getMeta(), new Meta());
    Meta newMeta = CanaryUtil.getOrElse(newTimeline.getMeta(), new Meta());
    Meta targetMeta = new Meta();
    targetMeta.setOldestId(CanaryUtil.getOrElse(oldMeta.getOldestId(), newMeta.getOldestId()));
    targetMeta.setNewestId(newMeta.getNewestId());
    targetMeta.setResultCount(
        CanaryUtil.getOrElse(oldMeta.getResultCount(), 0) + newMeta.getResultCount());
    targetMeta.setNextToken(newMeta.getNextToken());
    tl.setMeta(targetMeta);

    return tl;
  }

  private static Variant getMaxVariant(ArrayList<Variant> variants) {
    Variant maxVariant = new Variant(null, "", "");
    long temp = 0L;
    for (Variant variant : variants) {
      if (variant.getContentType().equals("video/mp4")) {
        Long maxTemp = variant.getBitRate();
        if (maxTemp > temp) {
          temp = maxTemp;
          maxVariant = variant;
        }
      }
    }
    return maxVariant;
  }

  private static String getPlaceFullname(final Place place) {
    ResponseGeoEntity respGeo;
    if (place.getContainedWithin() != null && place.getContainedWithin().size() == 1) {
      respGeo =
          new ResponseGeoEntity(
              place.getFullName(), place.getContainedWithin().get(0).getFullName());
    } else {
      respGeo = new ResponseGeoEntity(place.getFullName(), place.getCountry());
    }

    return respGeo.toString();
  }

  // Merge PublicMetrics for every tweet
  private static ResponseMetricsEntity convertMetrics(
      PublicMetrics tweetPublicMetrics, PublicMetrics userPublicMetrics) {
    ResponseMetricsEntity metrics = new ResponseMetricsEntity();

    if (tweetPublicMetrics != null) {
      metrics.setStatusMetrics(tweetPublicMetrics.getRetweetCount(),
          tweetPublicMetrics.getReplyCount(), tweetPublicMetrics.getLikeCount(),
          tweetPublicMetrics.getQuoteCount());
    }

    if (userPublicMetrics != null) {
      metrics.setUserMetrics(userPublicMetrics.getFollowersCount(),
          userPublicMetrics.getFollowingCount(), userPublicMetrics.getTweetCount(),
          userPublicMetrics.getListedCount(), userPublicMetrics.getViewCount());
    }

    return metrics;
  }

  // get media for single tweet
  private static ArrayList<ResponseMediaEntity> getMediaList(
      ArrayList<Media> medias, boolean maxVideo, boolean needPreview) {
    ArrayList<ResponseMediaEntity> mediaList = new ArrayList<>();
    if (medias == null || medias.isEmpty()) {
      return null;
    }

    medias.forEach(
        media -> {
          mediaList.addAll(getMedias(media, maxVideo, needPreview));
        });
    return mediaList;
  }

  // get media for multi-tweets
  // connect data.mediaKeys and includes.media with map
  private static ArrayList<ResponseMediaEntity> getMediaList(
      ArrayList<String> mediaKeys,
      HashMap<String, Media> mediaMap,
      boolean maxVideo,
      boolean needPreview) {
    ArrayList<ResponseMediaEntity> mediaList = new ArrayList<>();
    if (mediaKeys == null || mediaKeys.isEmpty() || mediaMap.isEmpty()) {
      return null;
    }

    mediaKeys.forEach(
        key -> {
          Media media = mediaMap.get(key);
          mediaList.addAll(getMedias(media, maxVideo, needPreview));
        });

    return mediaList;
  }

  private static ArrayList<ResponseMediaEntity> getMedias(
      Media media, boolean maxVideo, boolean needPreview) {
    ArrayList<ResponseMediaEntity> mediaList = new ArrayList<>();
    String key = media.getMediaKey();
    switch (media.getType()) {
      case "photo" -> {
        mediaList.add(new ResponseMediaEntity(key, "photo", null, media.getUrl()));
      }
      case "video" -> {
        if (media.getVariants() != null) {
          if (maxVideo) {
            Variant maxVariant = getMaxVariant(media.getVariants());
            mediaList.add(
                new ResponseMediaEntity(
                    key, "video", maxVariant.getBitRate(), maxVariant.getUrl()));
          } else {
            media
                .getVariants()
                .forEach(
                    variant -> {
                      if (variant.getContentType().equals("video/mp4")) {
                        mediaList.add(
                            new ResponseMediaEntity(
                                key, "video", variant.getBitRate(), variant.getUrl()));
                      }
                    });
          }
        }
        if (needPreview) {
          mediaList.add(
              new ResponseMediaEntity(key, "preview", null, (media.getPreviewImageUrl())));
        }
      }
      case "animated_gif" -> {
        mediaList.add(
            new ResponseMediaEntity(key, "animated_gif", 0L, media.getVariants().get(0).getUrl()));
        if (needPreview) {
          mediaList.add(
              new ResponseMediaEntity(key, "preview", null, (media.getPreviewImageUrl())));
        }
      }
      default -> log.warn("Unknown media type: " + media.getType());
    }
    return mediaList;
  }

  public static int parserTweetErrorType(String type) {
    type = TwitterUrlUtil.splitTwitterProblem(type);
    return switch (type) {
      case "invalid-request" -> 1400;
      case "resource-not-found" -> 1404;
      case "not-authorized-for-resource" -> 1401;
      case "disallowed-resource" -> 1403;
      case "about:blank" -> 1599;
      default -> 1500;
    };
  }

  private static HashMap<String, Media> convertMedia(List<Media> medias) {
    if (medias != null && medias.size() > 0) {
      return (HashMap<String, Media>) CollectionUtil.populateMap(medias, Media::getMediaKey);
    }
    return new HashMap<>();
  }

  private static HashMap<String, User> convertUser(List<User> users) {
    if (users != null && users.size() > 0) {
      return (HashMap<String, User>) CollectionUtil.populateMap(users, User::getId);
    }
    return new HashMap<>();
  }

  private static HashMap<String, Place> convertPlace(List<Place> places) {
    if (places != null && places.size() > 0) {
      return (HashMap<String, Place>) CollectionUtil.populateMap(places, Place::getId);
    }
    return new HashMap<>();
  }

  public static String tweetMediaUrlReplace(String type, String url) {
    return switch (type) {
      case "photo" -> tweetPhotoUrlReplace(url);
      case "preview" -> tweetPreviewUrlReplace(url);
      default -> url;
    };
  }

  private static String tweetPhotoUrlReplace(String photoUrl) throws NullPointerException {
    Objects.requireNonNull(photoUrl, "Photo URL must not null");
    if (StringUtil.nonText(photoUrl)) {
      return "";
    }

    if (TwitterUrlUtil.isTweetPhoto(photoUrl)) {
      String fileExten = ".jpg";
      if (photoUrl.endsWith(fileExten)) {
        return StringUtil.replaceBetween(
            photoUrl,
            photoUrl.length() - fileExten.length(),
            photoUrl.length(),
            "?format=jpg&name=4096x4096");
      }
      fileExten = ".png";
      if (photoUrl.endsWith(fileExten)) {
        return StringUtil.replaceBetween(
            photoUrl,
            photoUrl.length() - fileExten.length(),
            photoUrl.length(),
            "?format=png&name=4096x4096");
      }
    }
    return photoUrl;
  }

  private static String tweetPreviewUrlReplace(String previewUrl) throws NullPointerException {
    Objects.requireNonNull(previewUrl, "Preview URL must not null");
    if (StringUtil.nonText(previewUrl)) {
      return "";
    }

    if (TwitterUrlUtil.isTweetVideoThumb(previewUrl)) {
      String fileExten = ".jpg";
      if (previewUrl.endsWith(fileExten)) {
        return StringUtil.replaceBetween(
            previewUrl,
            previewUrl.length() - fileExten.length(),
            previewUrl.length(),
            "?format=jpg&name=large");
      }
      fileExten = ".png";
      if (previewUrl.endsWith(fileExten)) {
        return StringUtil.replaceBetween(
            previewUrl,
            previewUrl.length() - fileExten.length(),
            previewUrl.length(),
            "?format=png&name=large");
      }
    }
    return previewUrl;
  }

}
