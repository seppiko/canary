/*
 * Copyright 2023 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.seppiko.canary.models.metrics;

import java.util.List;
import java.util.Map;

/**
 * @author Leonard Woo
 */
public class RuntimeMXBean {

  private String name;

  private String pid;

  private String bootClassPath;

  private String libraryPath;

  private String clazzPath;

  private String specName;

  private String specVendor;

  private String specVersion;

  private String vmName;

  private String vmVendor;

  private String vmVersion;

  private String managementSpecVersion;

  private long startTime;

  private long uptime;

  private Map<String, String> systemProperties;

  private List<String> inputArguments;

  public RuntimeMXBean() {
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public String getPid() {
    return pid;
  }

  public void setPid(String pid) {
    this.pid = pid;
  }

  public String getBootClassPath() {
    return bootClassPath;
  }

  public void setBootClassPath(String bootClassPath) {
    this.bootClassPath = bootClassPath;
  }

  public String getLibraryPath() {
    return libraryPath;
  }

  public void setLibraryPath(String libraryPath) {
    this.libraryPath = libraryPath;
  }

  public String getClazzPath() {
    return clazzPath;
  }

  public void setClazzPath(String clazzPath) {
    this.clazzPath = clazzPath;
  }

  public String getSpecName() {
    return specName;
  }

  public void setSpecName(String specName) {
    this.specName = specName;
  }

  public String getSpecVendor() {
    return specVendor;
  }

  public void setSpecVendor(String specVendor) {
    this.specVendor = specVendor;
  }

  public String getSpecVersion() {
    return specVersion;
  }

  public void setSpecVersion(String specVersion) {
    this.specVersion = specVersion;
  }

  public String getVmName() {
    return vmName;
  }

  public void setVmName(String vmName) {
    this.vmName = vmName;
  }

  public String getVmVendor() {
    return vmVendor;
  }

  public void setVmVendor(String vmVendor) {
    this.vmVendor = vmVendor;
  }

  public String getVmVersion() {
    return vmVersion;
  }

  public void setVmVersion(String vmVersion) {
    this.vmVersion = vmVersion;
  }

  public String getManagementSpecVersion() {
    return managementSpecVersion;
  }

  public void setManagementSpecVersion(String managementSpecVersion) {
    this.managementSpecVersion = managementSpecVersion;
  }

  public long getStartTime() {
    return startTime;
  }

  public void setStartTime(long startTime) {
    this.startTime = startTime;
  }

  public long getUptime() {
    return uptime;
  }

  public void setUptime(long uptime) {
    this.uptime = uptime;
  }

  public Map<String, String> getSystemProperties() {
    return systemProperties;
  }

  public void setSystemProperties(Map<String, String> systemProperties) {
    this.systemProperties = systemProperties;
  }

  public List<String> getInputArguments() {
    return inputArguments;
  }

  public void setInputArguments(List<String> inputArguments) {
    this.inputArguments = inputArguments;
  }

}
