/*
 * Copyright 2023 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.seppiko.canary.models.metrics;

import java.lang.management.ThreadInfo;
import java.util.Map;

/**
 * @author Leonard Woo
 */
public class ThreadMXBean {

  private int threadCount;

  private int peakThreadCount;

  private long totalStartedThreadCount;

  private long daemonThreadCount;

  private Map<Long, ThreadInfo> allThreadInfo;

  private Map<Long, ThreadInfo> allDeadlockedThreadInfo;

  public ThreadMXBean() {
  }

  public int getThreadCount() {
    return threadCount;
  }

  public void setThreadCount(int threadCount) {
    this.threadCount = threadCount;
  }

  public int getPeakThreadCount() {
    return peakThreadCount;
  }

  public void setPeakThreadCount(int peakThreadCount) {
    this.peakThreadCount = peakThreadCount;
  }

  public long getTotalStartedThreadCount() {
    return totalStartedThreadCount;
  }

  public void setTotalStartedThreadCount(long totalStartedThreadCount) {
    this.totalStartedThreadCount = totalStartedThreadCount;
  }

  public long getDaemonThreadCount() {
    return daemonThreadCount;
  }

  public void setDaemonThreadCount(long daemonThreadCount) {
    this.daemonThreadCount = daemonThreadCount;
  }

  public Map<Long, ThreadInfo> getAllThreadInfo() {
    return allThreadInfo;
  }

  public void setAllThreadInfo(Map<Long, ThreadInfo> allThreadInfo) {
    this.allThreadInfo = allThreadInfo;
  }

  public Map<Long, ThreadInfo> getAllDeadlockedThreadInfo() {
    return allDeadlockedThreadInfo;
  }

  public void setAllDeadlockedThreadInfo(Map<Long, ThreadInfo> allDeadlockedThreadInfo) {
    this.allDeadlockedThreadInfo = allDeadlockedThreadInfo;
  }
}
