/*
 * Copyright 2023 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.seppiko.canary.models.metrics;

/**
 * @author Leonard Woo
 */
public class ClazzLoadingMXBean {

  private long totalLoadedCount;

  private int loadedCount;

  private long unloadedCount;

  public ClazzLoadingMXBean() {
  }

  public long getTotalLoadedCount() {
    return totalLoadedCount;
  }

  public void setTotalLoadedCount(long totalLoadedCount) {
    this.totalLoadedCount = totalLoadedCount;
  }

  public int getLoadedCount() {
    return loadedCount;
  }

  public void setLoadedCount(int loadedCount) {
    this.loadedCount = loadedCount;
  }

  public long getUnloadedCount() {
    return unloadedCount;
  }

  public void setUnloadedCount(long unloadedCount) {
    this.unloadedCount = unloadedCount;
  }
}
