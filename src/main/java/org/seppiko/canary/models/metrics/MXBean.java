/*
 * Copyright 2023 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.seppiko.canary.models.metrics;

import java.util.Properties;

/**
 * @author Leonard Woo
 */
public class MXBean {

  private RuntimeMXBean runtimeMXBean;

  private ClazzLoadingMXBean clazzLoadingMXBean;

  private ThreadMXBean threadMXBean;

  public MXBean() {
  }

  public RuntimeMXBean getRuntimeMXBean() {
    return runtimeMXBean;
  }

  public void setRuntimeMXBean(RuntimeMXBean runtimeMXBean) {
    this.runtimeMXBean = runtimeMXBean;
  }

  public ClazzLoadingMXBean getClazzLoadingMXBean() {
    return clazzLoadingMXBean;
  }

  public void setClazzLoadingMXBean(ClazzLoadingMXBean clazzLoadingMXBean) {
    this.clazzLoadingMXBean = clazzLoadingMXBean;
  }

  public ThreadMXBean getThreadMXBean() {
    return threadMXBean;
  }

  public void setThreadMXBean(ThreadMXBean threadMXBean) {
    this.threadMXBean = threadMXBean;
  }
}
