/*
 * Copyright 2023 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.seppiko.canary.models;

import com.fasterxml.jackson.annotation.JsonProperty;
import org.seppiko.commons.utils.StringUtil;

import java.util.ArrayList;

/**
 * @author Leonard Woo
 */
public record ResponseStatusEntity(@JsonProperty("id") String id,
                                   @JsonProperty("username") String username,
                                   @JsonProperty("nickname") String nickname,
                                   @JsonProperty("full_text") String fullText,
                                   @JsonProperty("created_at") String createdAt,
                                   @JsonProperty("place_full_name") String placeFullName,
                                   @JsonProperty("medias") ArrayList<ResponseMediaEntity> medias,
                                   @JsonProperty("metrics") ResponseMetricsEntity metrics) {

  @Override
  public String toString() {
    return String.format("%s (@%s): %s %s \n%s",
        nickname,
            username,
            StringUtil.unicodeDecode(fullText),
            (StringUtil.hasLength(placeFullName)? ("- at " + placeFullName): ""),
            createdAt);
  }
}
