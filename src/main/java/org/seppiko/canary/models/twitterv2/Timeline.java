/*
 * Copyright 2023 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.seppiko.canary.models.twitterv2;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.ArrayList;

/**
 * @author Leonard Woo
 */
public class Timeline implements Cloneable {

  @JsonProperty("data")
  private ArrayList<Data> data;

  @JsonProperty("includes")
  private Includes includes;

  @JsonProperty("meta")
  private Meta meta;

  @JsonProperty("errors")
  private ArrayList<Error> errors;

  @JsonProperty("title")
  private String title;

  @JsonProperty("detail")
  private String detail;

  @JsonProperty("type")
  private String type;

  public Timeline() {}

  public ArrayList<Data> getData() {
    return data;
  }

  public void setData(ArrayList<Data> data) {
    this.data = data;
  }

  public Includes getIncludes() {
    return includes;
  }

  public void setIncludes(Includes includes) {
    this.includes = includes;
  }

  public Meta getMeta() {
    return meta;
  }

  public void setMeta(Meta meta) {
    this.meta = meta;
  }

  public ArrayList<Error> getErrors() {
    return errors;
  }

  public void setErrors(ArrayList<Error> errors) {
    this.errors = errors;
  }

  public String getTitle() {
    return title;
  }

  public void setTitle(String title) {
    this.title = title;
  }

  public String getDetail() {
    return detail;
  }

  public void setDetail(String detail) {
    this.detail = detail;
  }

  public String getType() {
    return type;
  }

  public void setType(String type) {
    this.type = type;
  }

  @Override
  public Timeline clone() {
    try {
      return (Timeline) super.clone();
    } catch (CloneNotSupportedException e) {
      return this;
    }
  }
}
