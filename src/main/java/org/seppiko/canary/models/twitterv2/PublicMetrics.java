/*
 * Copyright 2023 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.seppiko.canary.models.twitterv2;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * @author Leonard Woo
 */
public class PublicMetrics {

  // Tweet Public Metrics
  @JsonProperty("retweet_count")
  private Integer retweetCount;

  @JsonProperty("reply_count")
  private Integer replyCount;

  @JsonProperty("like_count")
  private Integer likeCount;

  @JsonProperty("quote_count")
  private Integer quoteCount;

  // User Public Metrics
  @JsonProperty("followers_count")
  private Integer followersCount;

  @JsonProperty("following_count")
  private Integer followingCount;

  @JsonProperty("tweet_count")
  private Integer tweetCount;

  @JsonProperty("listed_count")
  private Integer listedCount;

  @JsonProperty("view_count")
  private Integer viewCount;

  public PublicMetrics() {
  }

  public Integer getRetweetCount() {
    return retweetCount;
  }

  public void setRetweetCount(Integer retweetCount) {
    this.retweetCount = retweetCount;
  }

  public Integer getReplyCount() {
    return replyCount;
  }

  public void setReplyCount(Integer replyCount) {
    this.replyCount = replyCount;
  }

  public Integer getLikeCount() {
    return likeCount;
  }

  public void setLikeCount(Integer likeCount) {
    this.likeCount = likeCount;
  }

  public Integer getQuoteCount() {
    return quoteCount;
  }

  public void setQuoteCount(Integer quoteCount) {
    this.quoteCount = quoteCount;
  }

  public Integer getFollowersCount() {
    return followersCount;
  }

  public void setFollowersCount(Integer followersCount) {
    this.followersCount = followersCount;
  }

  public Integer getFollowingCount() {
    return followingCount;
  }

  public void setFollowingCount(Integer followingCount) {
    this.followingCount = followingCount;
  }

  public Integer getTweetCount() {
    return tweetCount;
  }

  public void setTweetCount(Integer tweetCount) {
    this.tweetCount = tweetCount;
  }

  public Integer getListedCount() {
    return listedCount;
  }

  public void setListedCount(Integer listedCount) {
    this.listedCount = listedCount;
  }

  public Integer getViewCount() {
    return viewCount;
  }

  public void setViewCount(Integer viewCount) {
    this.viewCount = viewCount;
  }

}
