/*
 * Copyright 2023 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.seppiko.canary.models.twitterv2;

import com.fasterxml.jackson.annotation.JsonProperty;

public class Variant {

  @JsonProperty("bit_rate")
  private Long bitRate;

  @JsonProperty("content_type")
  private String contentType;

  @JsonProperty("url")
  private String url;

  public Variant() {}

  public Variant(Long bitRate, String contentType, String url) {
    this.bitRate = bitRate;
    this.contentType = contentType;
    this.url = url;
  }

  public Long getBitRate() {
    return bitRate;
  }

  public void setBitRate(Long bitRate) {
    this.bitRate = bitRate;
  }

  public String getContentType() {
    return contentType;
  }

  public void setContentType(String contentType) {
    this.contentType = contentType;
  }

  public String getUrl() {
    return url;
  }

  public void setUrl(String url) {
    this.url = url;
  }
}
