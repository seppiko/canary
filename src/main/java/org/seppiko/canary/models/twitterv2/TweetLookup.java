/*
 * Copyright 2023 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.seppiko.canary.models.twitterv2;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.ArrayList;

/**
 * @author Leonard Woo
 */
public class TweetLookup {

  @JsonProperty("data")
  private Data data;

  @JsonProperty("includes")
  private Includes includes;

  @JsonProperty("errors")
  private ArrayList<Error> errors;

  public TweetLookup() {
  }

  public Data getData() {
    return data;
  }

  public void setData(Data data) {
    this.data = data;
  }

  public Includes getIncludes() {
    return includes;
  }

  public void setIncludes(Includes includes) {
    this.includes = includes;
  }

  public ArrayList<Error> getErrors() {
    return errors;
  }

  public void setErrors(ArrayList<Error> errors) {
    this.errors = errors;
  }
}
