/*
 * Copyright 2023 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.seppiko.canary.models.twitterv2;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.ArrayList;

/**
 * @author Leonard Woo
 */
public class Entities {

  @JsonProperty("url")
  private UrlEntity url;

  @JsonProperty("urls")
  private ArrayList<Url> urls;

  @JsonProperty("mentions")
  private ArrayList<Mention> mentions;

  @JsonProperty("annotations")
  private ArrayList<Annotation> annotations;

  @JsonProperty("description")
  private Description description;

  @JsonProperty("hashtags")
  private ArrayList<Hashtag> hashtags;

  public Entities() {
  }

  public UrlEntity getUrl() {
    return url;
  }

  public void setUrl(UrlEntity url) {
    this.url = url;
  }

  public ArrayList<Url> getUrls() {
    return urls;
  }

  public void setUrls(ArrayList<Url> urls) {
    this.urls = urls;
  }

  public ArrayList<Mention> getMentions() {
    return mentions;
  }

  public void setMentions(ArrayList<Mention> mentions) {
    this.mentions = mentions;
  }

  public ArrayList<Annotation> getAnnotations() {
    return annotations;
  }

  public void setAnnotations(ArrayList<Annotation> annotations) {
    this.annotations = annotations;
  }

  public Description getDescription() {
    return description;
  }

  public void setDescription(Description description) {
    this.description = description;
  }

  public ArrayList<Hashtag> getHashtags() {
    return hashtags;
  }

  public void setHashtags(ArrayList<Hashtag> hashtags) {
    this.hashtags = hashtags;
  }

}
