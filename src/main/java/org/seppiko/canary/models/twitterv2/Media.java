/*
 * Copyright 2023 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.seppiko.canary.models.twitterv2;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.ArrayList;

/**
 * @author Leonard Woo
 */
public class Media {

  @JsonProperty("duration_ms")
  private Integer durationMs;

  @JsonProperty("width")
  private Integer width;

  @JsonProperty("preview_image_url")
  private String previewImageUrl;

  @JsonProperty("type")
  private String type;

  @JsonProperty("height")
  private Integer height;

  @JsonProperty("public_metrics")
  private PublicMetrics publicMetrics;

  @JsonProperty("media_key")
  private String mediaKey;

  @JsonProperty("url")
  private String url;

  @JsonProperty("variants")
  private ArrayList<Variant> variants;

  public Media() {
  }

  public Integer getDurationMs() {
    return durationMs;
  }

  public void setDurationMs(Integer durationMs) {
    this.durationMs = durationMs;
  }

  public Integer getWidth() {
    return width;
  }

  public void setWidth(Integer width) {
    this.width = width;
  }

  public String getPreviewImageUrl() {
    return previewImageUrl;
  }

  public void setPreviewImageUrl(String previewImageUrl) {
    this.previewImageUrl = previewImageUrl;
  }

  public String getType() {
    return type;
  }

  public void setType(String type) {
    this.type = type;
  }

  public Integer getHeight() {
    return height;
  }

  public void setHeight(Integer height) {
    this.height = height;
  }

  public PublicMetrics getPublicMetrics() {
    return publicMetrics;
  }

  public void setPublicMetrics(PublicMetrics publicMetrics) {
    this.publicMetrics = publicMetrics;
  }

  public String getMediaKey() {
    return mediaKey;
  }

  public void setMediaKey(String mediaKey) {
    this.mediaKey = mediaKey;
  }

  public String getUrl() {
    return url;
  }

  public void setUrl(String url) {
    this.url = url;
  }

  public ArrayList<Variant> getVariants() {
    return variants;
  }

  public void setVariants(ArrayList<Variant> variants) {
    this.variants = variants;
  }
}
