/*
 * Copyright 2023 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.seppiko.canary.models.twitterv2;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.ArrayList;

/**
 * @author Leonard Woo
 */
public class Url {

  @JsonProperty("start")
  private Integer start;
  @JsonProperty("end")
  private Integer end;
  @JsonProperty("url")
  private String url;
  @JsonProperty("expanded_url")
  private String expandedUrl;
  @JsonProperty("display_url")
  private String displayUrl;
  @JsonProperty("images")
  private ArrayList<Image> images;
  @JsonProperty("status")
  private Integer status;
  @JsonProperty("title")
  private String title;
  @JsonProperty("description")
  private String description;
  @JsonProperty("unwound_url")
  private String unwoundUrl;

  public Url() {
  }

  public Integer getStart() {
    return start;
  }

  public void setStart(Integer start) {
    this.start = start;
  }

  public Integer getEnd() {
    return end;
  }

  public void setEnd(Integer end) {
    this.end = end;
  }

  public String getUrl() {
    return url;
  }

  public void setUrl(String url) {
    this.url = url;
  }

  public String getExpandedUrl() {
    return expandedUrl;
  }

  public void setExpandedUrl(String expandedUrl) {
    this.expandedUrl = expandedUrl;
  }

  public String getDisplayUrl() {
    return displayUrl;
  }

  public void setDisplayUrl(String displayUrl) {
    this.displayUrl = displayUrl;
  }

  public ArrayList<Image> getImages() {
    return images;
  }

  public void setImages(ArrayList<Image> images) {
    this.images = images;
  }

  public Integer getStatus() {
    return status;
  }

  public void setStatus(Integer status) {
    this.status = status;
  }

  public String getTitle() {
    return title;
  }

  public void setTitle(String title) {
    this.title = title;
  }

  public String getDescription() {
    return description;
  }

  public void setDescription(String description) {
    this.description = description;
  }

  public String getUnwoundUrl() {
    return unwoundUrl;
  }

  public void setUnwoundUrl(String unwoundUrl) {
    this.unwoundUrl = unwoundUrl;
  }
}
