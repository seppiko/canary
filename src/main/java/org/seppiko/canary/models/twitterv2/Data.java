/*
 * Copyright 2023 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.seppiko.canary.models.twitterv2;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.ArrayList;

/**
 * @author Leonard Woo
 */
public class Data {

  @JsonProperty("id")
  private String id;

  @JsonProperty("text")
  private String text;

  @JsonProperty("entities")
  private Entities entities;

  @JsonProperty("lang")
  private String lang;

  @JsonProperty("conversation_id")
  private String conversationId;

  @JsonProperty("author_id")
  private String authorId;

  @JsonProperty("reply_settings")
  private String replySettings;

  @JsonProperty("attachments")
  private Attachments attachments;

  @JsonProperty("created_at")
  private String createdAt;

  @JsonProperty("source")
  private String source;

  @JsonProperty("possibly_sensitive")
  private Boolean possiblySensitive;

  @JsonProperty("context_annotations")
  private ArrayList<ContextAnnotation> contextAnnotations;

  @JsonProperty("in_reply_to_user_id")
  private String inReplyToUserId;

  @JsonProperty("geo")
  private Geo geo;

  @JsonProperty("public_metrics")
  private PublicMetrics publicMetrics;

  @JsonProperty("edit_history_tweet_ids")
  private ArrayList<String> editHistoryTweetIds;

  @JsonProperty("referenced_tweets")
  private ArrayList<ReferencedTweet> referencedTweets;

  public Data() {
  }

  public String getId() {
    return id;
  }

  public void setId(String id) {
    this.id = id;
  }

  public String getText() {
    return text;
  }

  public void setText(String text) {
    this.text = text;
  }

  public Entities getEntities() {
    return entities;
  }

  public void setEntities(Entities entities) {
    this.entities = entities;
  }

  public String getLang() {
    return lang;
  }

  public void setLang(String lang) {
    this.lang = lang;
  }

  public String getConversationId() {
    return conversationId;
  }

  public void setConversationId(String conversationId) {
    this.conversationId = conversationId;
  }

  public String getAuthorId() {
    return authorId;
  }

  public void setAuthorId(String authorId) {
    this.authorId = authorId;
  }

  public String getReplySettings() {
    return replySettings;
  }

  public void setReplySettings(String replySettings) {
    this.replySettings = replySettings;
  }

  public Attachments getAttachments() {
    return attachments;
  }

  public void setAttachments(Attachments attachments) {
    this.attachments = attachments;
  }

  public String getCreatedAt() {
    return createdAt;
  }

  public void setCreatedAt(String createdAt) {
    this.createdAt = createdAt;
  }

  public String getSource() {
    return source;
  }

  public void setSource(String source) {
    this.source = source;
  }

  public Boolean getPossiblySensitive() {
    return possiblySensitive;
  }

  public void setPossiblySensitive(Boolean possiblySensitive) {
    this.possiblySensitive = possiblySensitive;
  }

  public ArrayList<ContextAnnotation> getContextAnnotations() {
    return contextAnnotations;
  }

  public void setContextAnnotations(ArrayList<ContextAnnotation> contextAnnotations) {
    this.contextAnnotations = contextAnnotations;
  }

  public String getInReplyToUserId() {
    return inReplyToUserId;
  }

  public void setInReplyToUserId(String inReplyToUserId) {
    this.inReplyToUserId = inReplyToUserId;
  }

  public Geo getGeo() {
    return geo;
  }

  public void setGeo(Geo geo) {
    this.geo = geo;
  }

  public PublicMetrics getPublicMetrics() {
    return publicMetrics;
  }

  public void setPublicMetrics(PublicMetrics publicMetrics) {
    this.publicMetrics = publicMetrics;
  }

  public ArrayList<String> getEditHistoryTweetIds() {
    return editHistoryTweetIds;
  }

  public void setEditHistoryTweetIds(ArrayList<String> editHistoryTweetIds) {
    this.editHistoryTweetIds = editHistoryTweetIds;
  }

  public ArrayList<ReferencedTweet> getReferencedTweets() {
    return referencedTweets;
  }

  public void setReferencedTweets(ArrayList<ReferencedTweet> referencedTweets) {
    this.referencedTweets = referencedTweets;
  }
}
