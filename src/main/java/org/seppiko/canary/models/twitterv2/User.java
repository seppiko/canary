/*
 * Copyright 2023 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.seppiko.canary.models.twitterv2;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * @author Leonard Woo
 */
public class User {

  @JsonProperty("id")
  private String id;
  @JsonProperty("location")
  private String location;
  @JsonProperty("pinned_tweet_id")
  private String pinnedTweetId;
  @JsonProperty("description")
  private String description;
  @JsonProperty("name")
  private String name;
  @JsonProperty("url")
  private String url;
  @JsonProperty("protected")
  private Boolean _protected;
  @JsonProperty("profile_image_url")
  private String profileImageUrl;
  @JsonProperty("verified")
  private Boolean verified;
  @JsonProperty("created_at")
  private String createdAt;
  @JsonProperty("public_metrics")
  private PublicMetrics publicMetrics;
  @JsonProperty("username")
  private String username;
  @JsonProperty("entities")
  private Entities entities;

  public User() {
  }

  public String getId() {
    return id;
  }

  public void setId(String id) {
    this.id = id;
  }

  public String getLocation() {
    return location;
  }

  public void setLocation(String location) {
    this.location = location;
  }

  public String getPinnedTweetId() {
    return pinnedTweetId;
  }

  public void setPinnedTweetId(String pinnedTweetId) {
    this.pinnedTweetId = pinnedTweetId;
  }

  public String getDescription() {
    return description;
  }

  public void setDescription(String description) {
    this.description = description;
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public String getUrl() {
    return url;
  }

  public void setUrl(String url) {
    this.url = url;
  }

  public Boolean getProtected() {
    return _protected;
  }

  public void setProtected(Boolean _protected) {
    this._protected = _protected;
  }

  public String getProfileImageUrl() {
    return profileImageUrl;
  }

  public void setProfileImageUrl(String profileImageUrl) {
    this.profileImageUrl = profileImageUrl;
  }

  public Boolean getVerified() {
    return verified;
  }

  public void setVerified(Boolean verified) {
    this.verified = verified;
  }

  public String getCreatedAt() {
    return createdAt;
  }

  public void setCreatedAt(String createdAt) {
    this.createdAt = createdAt;
  }

  public PublicMetrics getPublicMetrics() {
    return publicMetrics;
  }

  public void setPublicMetrics(PublicMetrics publicMetrics) {
    this.publicMetrics = publicMetrics;
  }

  public String getUsername() {
    return username;
  }

  public void setUsername(String username) {
    this.username = username;
  }

  public Entities getEntities() {
    return entities;
  }

  public void setEntities(Entities entities) {
    this.entities = entities;
  }

}
