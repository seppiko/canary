/*
 * Copyright 2023 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.seppiko.canary.models.twitterv2;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.List;

/**
 * @author Leonard Woo
 */
public class Place {

  @JsonProperty("full_name")
  private String fullName;

  @JsonProperty("id")
  private String id;

  @JsonProperty("country")
  private String country;

  @JsonProperty("country_code")
  private String countryCode;

  @JsonProperty("geo")
  private Geo geo;

  @JsonProperty("name")
  private String name;

  @JsonProperty("place_type")
  private String placeType;

  @JsonProperty("contained_within")
  private List<Place> containedWithin;

  public Place() {
  }

  public String getFullName() {
    return fullName;
  }

  public void setFullName(String fullName) {
    this.fullName = fullName;
  }

  public String getId() {
    return id;
  }

  public void setId(String id) {
    this.id = id;
  }

  public String getCountry() {
    return country;
  }

  public void setCountry(String country) {
    this.country = country;
  }

  public String getCountryCode() {
    return countryCode;
  }

  public void setCountryCode(String countryCode) {
    this.countryCode = countryCode;
  }

  public Geo getGeo() {
    return geo;
  }

  public void setGeo(Geo geo) {
    this.geo = geo;
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public String getPlaceType() {
    return placeType;
  }

  public void setPlaceType(String placeType) {
    this.placeType = placeType;
  }

  public List<Place> getContainedWithin() {
    return containedWithin;
  }

  public void setContainedWithin(List<Place> containedWithin) {
    this.containedWithin = containedWithin;
  }
}
