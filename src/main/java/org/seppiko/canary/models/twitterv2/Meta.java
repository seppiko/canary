/*
 * Copyright 2023 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.seppiko.canary.models.twitterv2;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * @author Leonard Woo
 */
public class Meta {

  @JsonProperty("oldest_id")
  private String oldestId;
  @JsonProperty("newest_id")
  private String newestId;
  @JsonProperty("result_count")
  private Integer resultCount;
  @JsonProperty("next_token")
  private String nextToken;

  public Meta() {}

  public String getOldestId() {
    return oldestId;
  }

  public void setOldestId(String oldestId) {
    this.oldestId = oldestId;
  }

  public String getNewestId() {
    return newestId;
  }

  public void setNewestId(String newestId) {
    this.newestId = newestId;
  }

  public Integer getResultCount() {
    return resultCount;
  }

  public void setResultCount(Integer resultCount) {
    this.resultCount = resultCount;
  }

  public String getNextToken() {
    return nextToken;
  }

  public void setNextToken(String nextToken) {
    this.nextToken = nextToken;
  }

}
