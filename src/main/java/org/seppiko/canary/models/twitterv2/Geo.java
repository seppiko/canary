/*
 * Copyright 2023 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.seppiko.canary.models.twitterv2;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.List;

/**
 * @author Leonard Woo
 */
public class Geo {

  @JsonProperty("place_id")
  private String placeId;

  @JsonProperty("type")
  private String type;

  @JsonProperty("bbox")
  private List<Integer> bbox;

  @JsonProperty("properties")
  private Object properties;

  public Geo() {
  }

  public String getPlaceId() {
    return placeId;
  }

  public void setPlaceId(String placeId) {
    this.placeId = placeId;
  }

  public String getType() {
    return type;
  }

  public void setType(String type) {
    this.type = type;
  }

  public List<Integer> getBbox() {
    return bbox;
  }

  public void setBbox(List<Integer> bbox) {
    this.bbox = bbox;
  }

  public Object getProperties() {
    return properties;
  }

  public void setProperties(Object properties) {
    this.properties = properties;
  }
}
