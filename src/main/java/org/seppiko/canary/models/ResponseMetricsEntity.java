/*
 * Copyright 2023 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.seppiko.canary.models;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * @author Leonard Woo
 */
public class ResponseMetricsEntity {

  // Status Metrics
  @JsonProperty("forward_count")
  private Integer forwardCount;

  @JsonProperty("reply_count")
  private Integer replyCount;

  @JsonProperty("like_count")
  private Integer likeCount;

  @JsonProperty("quote_count")
  private Integer quoteCount;

  // User Metrics
  @JsonProperty("followers_count")
  private Integer followersCount;

  @JsonProperty("following_count")
  private Integer followingCount;

  @JsonProperty("statuses_count")
  private Integer statusesCount;

  @JsonProperty("listed_count")
  private Integer listedCount;

  @JsonProperty("view_count")
  private Integer viewCount;

  public ResponseMetricsEntity() {
  }

  public void setStatusMetrics(Integer forwardCount, Integer replyCount, Integer likeCount,
      Integer quoteCount) {
    this.forwardCount = forwardCount;
    this.replyCount = replyCount;
    this.likeCount = likeCount;
    this.quoteCount = quoteCount;
  }

  public void setUserMetrics(Integer followersCount, Integer followingCount, Integer statusesCount,
      Integer listedCount, Integer viewCount) {
    this.followersCount = followersCount;
    this.followingCount = followingCount;
    this.statusesCount = statusesCount;
    this.listedCount = listedCount;
    this.viewCount = viewCount;
  }

  public Integer getForwardCount() {
    return forwardCount;
  }

  public void setForwardCount(Integer forwardCount) {
    this.forwardCount = forwardCount;
  }

  public Integer getReplyCount() {
    return replyCount;
  }

  public void setReplyCount(Integer replyCount) {
    this.replyCount = replyCount;
  }

  public Integer getLikeCount() {
    return likeCount;
  }

  public void setLikeCount(Integer likeCount) {
    this.likeCount = likeCount;
  }

  public Integer getQuoteCount() {
    return quoteCount;
  }

  public void setQuoteCount(Integer quoteCount) {
    this.quoteCount = quoteCount;
  }

  public Integer getFollowersCount() {
    return followersCount;
  }

  public void setFollowersCount(Integer followersCount) {
    this.followersCount = followersCount;
  }

  public Integer getFollowingCount() {
    return followingCount;
  }

  public void setFollowingCount(Integer followingCount) {
    this.followingCount = followingCount;
  }

  public Integer getStatusesCount() {
    return statusesCount;
  }

  public void setStatusesCount(Integer statusesCount) {
    this.statusesCount = statusesCount;
  }

  public Integer getListedCount() {
    return listedCount;
  }

  public void setListedCount(Integer listedCount) {
    this.listedCount = listedCount;
  }

  public Integer getViewCount() {
    return viewCount;
  }

  public void setViewCount(Integer viewCount) {
    this.viewCount = viewCount;
  }
}
