/*
 * Copyright 2023 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.seppiko.canary.models;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * @author Leonard Woo
 */
public record RequestTwitterJsonEntity(@JsonProperty("tweet_ids") String tweetIds,
                                       @JsonProperty("username") String username,
                                       @JsonProperty("count") Integer count,
                                       @JsonProperty("until_id") String untilId,
                                       @JsonProperty("since_id") String sinceId,
                                       @JsonProperty("preview") Boolean preview,
                                       @JsonProperty("only_max_media") Boolean onlyMaxMedia,
                                       @JsonProperty("download_media") Boolean downloadMedia) {
}
