/*
 * Copyright 2023 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.seppiko.canary.models;

import com.fasterxml.jackson.annotation.JsonProperty;
import org.seppiko.commons.utils.StringUtil;

import java.util.ArrayList;
import java.util.Vector;

/**
 * @author Leonard Woo
 */
public class ResponseTimelineEntity {

  @JsonProperty("statuses")
  private Vector<ResponseStatusEntity> statuses;

  @JsonProperty("until_id")
  private String untilId;

  @JsonProperty("since_id")
  private String sinceId;

  @JsonProperty("count")
  private Integer count;

  public ResponseTimelineEntity() {
    statuses = new Vector<>();
    sinceId = "";
    untilId = "";
    count = 0;
  }

  public ArrayList<ResponseStatusEntity> getStatuses() {
    return new ArrayList<>(statuses);
  }

  public void setStatuses(ArrayList<ResponseStatusEntity> statuses) {
    this.statuses = new Vector<>(statuses);
    count = statuses.size();
  }

  public String getUntilId() {
    return untilId;
  }

  public void setUntilId(String untilId) {
    this.untilId = untilId;
  }

  public String getSinceId() {
    return sinceId;
  }

  public void setSinceId(String sinceId) {
    this.sinceId = sinceId;
  }

  public Integer getCount() {
    return count;
  }

  public void setCount(Integer count) {
    this.count = count;
  }

  public void addAll(ResponseTimelineEntity respTimeline) {
    statuses.addAll(respTimeline.statuses);
    if (sinceId == null || sinceId.isEmpty()) {
      sinceId = respTimeline.sinceId;
    } else {
      sinceId += "," + respTimeline.sinceId;
    }
    if (untilId == null || untilId.isEmpty()) {
      untilId = respTimeline.untilId;
    } else {
      untilId += "," + respTimeline;
    }
    if (count == null || count <= 0) {
      count = respTimeline.count;
    } else {
      count += respTimeline.count;
    }
  }

  public boolean _isEmpty() throws NullPointerException {
    return statuses.isEmpty() && StringUtil.isNullOrEmpty(sinceId) && StringUtil.isNullOrEmpty(untilId);
  }

  public void _clear() {
    statuses.clear();
    sinceId = "";
    untilId = "";
    count = 0;
  }

  public void _onlyClear() {
    statuses.clear();
  }
}
