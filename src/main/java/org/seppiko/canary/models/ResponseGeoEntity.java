/*
 * Copyright 2023 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.seppiko.canary.models;

import org.seppiko.canary.utils.CanaryUtil;
import org.seppiko.commons.utils.StringUtil;

/**
 * @author Leonard Woo
 */
public record ResponseGeoEntity(String placeFullName, String regionFullName) {

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append(placeFullName);
    if (StringUtil.hasLength(regionFullName)) {
      String[] region = CanaryUtil.regexArray(regionFullName, ", ");
      String splitChar = ", ";
      if (!(CanaryUtil.search(placeFullName, region[0]))) {
        sb.append(splitChar);
        sb.append(regionFullName);
      } else if (!placeFullName.contains(region[1])) {
        sb.append(splitChar);
        sb.append(region[1]);
      }
    }
    return sb.toString();
  }
}
