/*
 * Copyright 2023 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.seppiko.canary.models.mastodon;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * @author Leonard Woo
 */
public class AccountLookup {

  public AccountLookup() {}

  @JsonProperty("accounts")
  private Account[] accounts;

  @JsonProperty("statuses")
  private TootLookup[] statuses;

  @JsonProperty("hashtags")
  private Object[] hashtags;

  public Account[] getAccounts() {
    return accounts;
  }

  public void setAccounts(Account[] accounts) {
    this.accounts = accounts;
  }

  public TootLookup[] getStatuses() {
    return statuses;
  }

  public void setStatuses(TootLookup[] statuses) {
    this.statuses = statuses;
  }

  public Object[] getHashtags() {
    return hashtags;
  }

  public void setHashtags(Object[] hashtags) {
    this.hashtags = hashtags;
  }
}
