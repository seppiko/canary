/*
 * Copyright 2023 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.seppiko.canary.models.mastodon;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * @author Leonard Woo
 */
public class TootLookup {

  public TootLookup() {}

  @JsonProperty("id")
  private String id;

  @JsonProperty("created_at")
  private String createdAt;

  @JsonProperty("in_reply_to_id")
  private String inReplyToId;

  @JsonProperty("in_reply_to_account_id")
  private String inReplyToAccountId;

  @JsonProperty("sensitive")
  private Boolean sensitive;

  @JsonProperty("spoiler_text")
  private String spoilerText;

  @JsonProperty("visibility")
  private String visibility;

  @JsonProperty("language")
  private String language;

  @JsonProperty("uri")
  private String uri;

  @JsonProperty("url")
  private String url;

  @JsonProperty("replies_count")
  private Integer repliesCount;

  @JsonProperty("reblogs_count")
  private Integer reblogsCount;

  @JsonProperty("favourites_count")
  private Integer favouritesCount;

  @JsonProperty("edited_at")
  private String editedAt;

  @JsonProperty("favourited")
  private Boolean favourited;

  @JsonProperty("reblogged")
  private Boolean reblogged;

  @JsonProperty("muted")
  private Boolean muted;

  @JsonProperty("bookmarked")
  private Boolean bookmarked;

  @JsonProperty("pinned")
  private Boolean pinned;

  @JsonProperty("content")
  private String content;

  @JsonProperty("filtered")
  private Object[] filtered;

  @JsonProperty("reblog")
  private Object reblog;

  @JsonProperty("application")
  private Application application;

  @JsonProperty("account")
  private Account account;

  @JsonProperty("media_attachments")
  private Media[] mediaAttachments;

  @JsonProperty("mentions")
  private Object[] mentions;

  @JsonProperty("tags")
  private Object[] tags;

  @JsonProperty("emojis")
  private Emoji[] emojis;

  @JsonProperty("card")
  private Card card;

  @JsonProperty("poll")
  private Object poll;

  @JsonProperty("error")
  private String error;

  public String getId() {
    return id;
  }

  public void setId(String id) {
    this.id = id;
  }

  public String getCreatedAt() {
    return createdAt;
  }

  public void setCreatedAt(String createdAt) {
    this.createdAt = createdAt;
  }

  public String getInReplyToId() {
    return inReplyToId;
  }

  public void setInReplyToId(String inReplyToId) {
    this.inReplyToId = inReplyToId;
  }

  public String getInReplyToAccountId() {
    return inReplyToAccountId;
  }

  public void setInReplyToAccountId(String inReplyToAccountId) {
    this.inReplyToAccountId = inReplyToAccountId;
  }

  public Boolean getSensitive() {
    return sensitive;
  }

  public void setSensitive(Boolean sensitive) {
    this.sensitive = sensitive;
  }

  public String getSpoilerText() {
    return spoilerText;
  }

  public void setSpoilerText(String spoilerText) {
    this.spoilerText = spoilerText;
  }

  public String getVisibility() {
    return visibility;
  }

  public void setVisibility(String visibility) {
    this.visibility = visibility;
  }

  public String getLanguage() {
    return language;
  }

  public void setLanguage(String language) {
    this.language = language;
  }

  public String getUri() {
    return uri;
  }

  public void setUri(String uri) {
    this.uri = uri;
  }

  public String getUrl() {
    return url;
  }

  public void setUrl(String url) {
    this.url = url;
  }

  public Integer getRepliesCount() {
    return repliesCount;
  }

  public void setRepliesCount(Integer repliesCount) {
    this.repliesCount = repliesCount;
  }

  public Integer getReblogsCount() {
    return reblogsCount;
  }

  public void setReblogsCount(Integer reblogsCount) {
    this.reblogsCount = reblogsCount;
  }

  public Integer getFavouritesCount() {
    return favouritesCount;
  }

  public void setFavouritesCount(Integer favouritesCount) {
    this.favouritesCount = favouritesCount;
  }

  public String getEditedAt() {
    return editedAt;
  }

  public void setEditedAt(String editedAt) {
    this.editedAt = editedAt;
  }

  public Boolean getFavourited() {
    return favourited;
  }

  public void setFavourited(Boolean favourited) {
    this.favourited = favourited;
  }

  public Boolean getReblogged() {
    return reblogged;
  }

  public void setReblogged(Boolean reblogged) {
    this.reblogged = reblogged;
  }

  public Boolean getMuted() {
    return muted;
  }

  public void setMuted(Boolean muted) {
    this.muted = muted;
  }

  public Boolean getBookmarked() {
    return bookmarked;
  }

  public void setBookmarked(Boolean bookmarked) {
    this.bookmarked = bookmarked;
  }

  public Boolean getPinned() {
    return pinned;
  }

  public void setPinned(Boolean pinned) {
    this.pinned = pinned;
  }

  public String getContent() {
    return content;
  }

  public void setContent(String content) {
    this.content = content;
  }

  public Object[] getFiltered() {
    return filtered;
  }

  public void setFiltered(Object[] filtered) {
    this.filtered = filtered;
  }

  public Object getReblog() {
    return reblog;
  }

  public void setReblog(Object reblog) {
    this.reblog = reblog;
  }

  public Application getApplication() {
    return application;
  }

  public void setApplication(Application application) {
    this.application = application;
  }

  public Account getAccount() {
    return account;
  }

  public void setAccount(Account account) {
    this.account = account;
  }

  public Media[] getMediaAttachments() {
    return mediaAttachments;
  }

  public void setMediaAttachments(Media[] mediaAttachments) {
    this.mediaAttachments = mediaAttachments;
  }

  public Object[] getMentions() {
    return mentions;
  }

  public void setMentions(Object[] mentions) {
    this.mentions = mentions;
  }

  public Object[] getTags() {
    return tags;
  }

  public void setTags(Object[] tags) {
    this.tags = tags;
  }

  public Emoji[] getEmojis() {
    return emojis;
  }

  public void setEmojis(Emoji[] emojis) {
    this.emojis = emojis;
  }

  public Card getCard() {
    return card;
  }

  public void setCard(Card card) {
    this.card = card;
  }

  public Object getPoll() {
    return poll;
  }

  public void setPoll(Object poll) {
    this.poll = poll;
  }

  public String getError() {
    return error;
  }

  public void setError(String error) {
    this.error = error;
  }
}
