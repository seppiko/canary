/*
 * Copyright 2023 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.seppiko.canary.models.mastodon;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * @see <a href="https://docs.joinmastodon.org/entities/Account/">Account</a>
 * @author Leonard Woo
 */
public class Account {

  public Account() {}

  @JsonProperty("id")
  private String id;

  @JsonProperty("username")
  private String username;

  @JsonProperty("acct")
  private String acct;

  @JsonProperty("display_name")
  private String displayName;

  @JsonProperty("locked")
  private Boolean locked;

  @JsonProperty("bot")
  private Boolean bot;

  @JsonProperty("discoverable")
  private Boolean discoverable;

  @JsonProperty("group")
  private Boolean group;

  @JsonProperty("created_at")
  private String createdAt;

  @JsonProperty("note")
  private String note;

  @JsonProperty("url")
  private String url;

  @JsonProperty("avatar")
  private String avatar;

  @JsonProperty("avatar_static")
  private String avatarStatic;

  @JsonProperty("header")
  private String header;

  @JsonProperty("header_static")
  private String headerStatic;

  @JsonProperty("followers_count")
  private Integer followersCount;

  @JsonProperty("following_count")
  private Integer followingCount;

  @JsonProperty("statuses_count")
  private Integer statusesCount;

  @JsonProperty("last_status_at")
  private String lastStatusAt;

  @JsonProperty("noindex")
  private Boolean noindex;

  @JsonProperty("emojis")
  private Emoji[] emojis;

  @JsonProperty("fields")
  private Field[] fields;

  public String getId() {
    return id;
  }

  public void setId(String id) {
    this.id = id;
  }

  public String getUsername() {
    return username;
  }

  public void setUsername(String username) {
    this.username = username;
  }

  public String getAcct() {
    return acct;
  }

  public void setAcct(String acct) {
    this.acct = acct;
  }

  public String getDisplayName() {
    return displayName;
  }

  public void setDisplayName(String displayName) {
    this.displayName = displayName;
  }

  public Boolean getLocked() {
    return locked;
  }

  public void setLocked(Boolean locked) {
    this.locked = locked;
  }

  public Boolean getBot() {
    return bot;
  }

  public void setBot(Boolean bot) {
    this.bot = bot;
  }

  public Boolean getDiscoverable() {
    return discoverable;
  }

  public void setDiscoverable(Boolean discoverable) {
    this.discoverable = discoverable;
  }

  public Boolean getGroup() {
    return group;
  }

  public void setGroup(Boolean group) {
    this.group = group;
  }

  public String getCreatedAt() {
    return createdAt;
  }

  public void setCreatedAt(String createdAt) {
    this.createdAt = createdAt;
  }

  public String getNote() {
    return note;
  }

  public void setNote(String note) {
    this.note = note;
  }

  public String getUrl() {
    return url;
  }

  public void setUrl(String url) {
    this.url = url;
  }

  public String getAvatar() {
    return avatar;
  }

  public void setAvatar(String avatar) {
    this.avatar = avatar;
  }

  public String getAvatarStatic() {
    return avatarStatic;
  }

  public void setAvatarStatic(String avatarStatic) {
    this.avatarStatic = avatarStatic;
  }

  public String getHeader() {
    return header;
  }

  public void setHeader(String header) {
    this.header = header;
  }

  public String getHeaderStatic() {
    return headerStatic;
  }

  public void setHeaderStatic(String headerStatic) {
    this.headerStatic = headerStatic;
  }

  public Integer getFollowersCount() {
    return followersCount;
  }

  public void setFollowersCount(Integer followersCount) {
    this.followersCount = followersCount;
  }

  public Integer getFollowingCount() {
    return followingCount;
  }

  public void setFollowingCount(Integer followingCount) {
    this.followingCount = followingCount;
  }

  public Integer getStatusesCount() {
    return statusesCount;
  }

  public void setStatusesCount(Integer statusesCount) {
    this.statusesCount = statusesCount;
  }

  public String getLastStatusAt() {
    return lastStatusAt;
  }

  public void setLastStatusAt(String lastStatusAt) {
    this.lastStatusAt = lastStatusAt;
  }

  public Boolean getNoindex() {
    return noindex;
  }

  public void setNoindex(Boolean noindex) {
    this.noindex = noindex;
  }

  public Emoji[] getEmojis() {
    return emojis;
  }

  public void setEmojis(Emoji[] emojis) {
    this.emojis = emojis;
  }

  public Field[] getFields() {
    return fields;
  }

  public void setFields(Field[] fields) {
    this.fields = fields;
  }
}
