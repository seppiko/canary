/*
 * Copyright 2023 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.seppiko.canary.configures;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 *
 * @author Leonard Woo
 */
public class APIEntity {

  public APIEntity() {
  }

  @JsonProperty("server")
  private String server;

  @JsonProperty("token")
  private String token;

  @JsonProperty("timeout")
  private Integer timeout;

  @JsonProperty("number_of_retries")
  private Integer numberOfRetries;

  @JsonProperty("retry_interval_seconds")
  private Integer retryIntervalSeconds;

  @JsonProperty("multiple_intervals_seconds")
  private Integer multipleIntervalsSeconds;

  public String getServer() {
    return server;
  }

  public void setServer(String server) {
    this.server = server;
  }

  public String getToken() {
    return token;
  }

  public void setToken(String token) {
    this.token = token;
  }

  public Integer getTimeout() {
    return timeout;
  }

  public void setTimeout(Integer timeout) {
    this.timeout = timeout;
  }

  public int getMultipleIntervalsSeconds() {
    return multipleIntervalsSeconds;
  }

  public void setMultipleIntervalsSeconds(Integer multipleIntervalsSeconds) {
    this.multipleIntervalsSeconds = multipleIntervalsSeconds;
  }

  public Integer getNumberOfRetries() {
    return numberOfRetries;
  }

  public void setNumberOfRetries(Integer numberOfRetries) {
    this.numberOfRetries = numberOfRetries;
  }

  public Integer getRetryIntervalSeconds() {
    return retryIntervalSeconds;
  }

  public void setRetryIntervalSeconds(Integer retryIntervalSeconds) {
    this.retryIntervalSeconds = retryIntervalSeconds;
  }
}
