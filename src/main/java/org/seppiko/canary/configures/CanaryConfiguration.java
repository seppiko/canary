/*
 * Copyright 2023 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.seppiko.canary.configures;

import com.fasterxml.jackson.databind.JsonNode;
import org.seppiko.canary.utils.JsonUtil;
import org.seppiko.canary.utils.VersionFactory;
import org.seppiko.commons.logging.Logging;
import org.seppiko.commons.logging.LoggingFactory;
import org.seppiko.commons.utils.Environment;
import org.seppiko.commons.utils.StreamUtil;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.InputStream;
import java.util.Objects;

/**
 * Canary Configuration
 *
 * @author Leonard Woo
 */
public class CanaryConfiguration {

  private static final CanaryConfiguration INSTANCE = new CanaryConfiguration();

  public static CanaryConfiguration getInstance() {
    return INSTANCE;
  }

  private CanaryConfiguration() {
    init();
  }

  private final Logging log = LoggingFactory.getLogging(CanaryConfiguration.class);

  public static final String UA = "SeppikoCanary/" + VersionFactory.VERSION;

  public static final String FILE_SEPARATOR = System.getProperty("file.separator");

  private void init() {
    try {
      // Load config file
      String filepath = System.getProperty("canary.configFile", Environment.CONFIG_FILENAME_JSON);
      InputStream is = StreamUtil.getStream(filepath);
      if(Objects.isNull(is)) {
        throw new FileNotFoundException("config.json not found");
      }
      BufferedReader reader = StreamUtil.loadReader( is );

      // Parser config file
      JsonNode root = JsonUtil.fromJsonObject(reader);
      Objects.requireNonNull(root);
      log.info("Config: " + root);
      loadConfig(root);
      checkConfig();
    } catch (Exception ex) {
      String msg = "Configuration initialization failed";
      log.atError().message(msg).withCause(ex).log();
      throw new RuntimeException(msg);
    }
  }

  private String host;

  public String getHost() {
    return host;
  }

  private int port;

  public int getPort() {
    return port;
  }

  private StorageEntity storage;

  public StorageEntity getStorage() {
    return storage;
  }

  private ProxyEntity proxy;

  public ProxyEntity getProxy() {
    return proxy;
  }

  private APIEntity twitter;

  public APIEntity getTwitter() {
    return twitter;
  }

  private DownloadEntity download;

  public DownloadEntity getDownload() {
    return download;
  }

  private APIEntity mastodon;

  public APIEntity getMastodon() {
    return mastodon;
  }

  private void loadConfig(JsonNode root) {
    this.host = root.get("host").asText("0.0.0.0");
    this.port = root.get("port").asInt(8700);

    JsonNode storage = root.get("storage");
    this.storage = (storage.isNull() || storage.isEmpty())? null: JsonUtil.toT(storage, StorageEntity.class);

    JsonNode proxy = root.get("proxy");
    this.proxy = (proxy.isNull() || proxy.isEmpty())? null: JsonUtil.toT(proxy, ProxyEntity.class);

    if (root.has("twitter")) {
      JsonNode twitter = root.get("twitter");
      this.twitter =
          (twitter.isNull() || twitter.isEmpty()) ? null : JsonUtil.toT(twitter, APIEntity.class);
    } else {
      this.twitter = null;
    }

    JsonNode download = root.get("download");
    this.download = (download.isNull() || download.isEmpty())? null: JsonUtil.toT(download, DownloadEntity.class);

    if (root.has("mastodon")) {
      JsonNode mastodon = root.get("mastodon");
      this.mastodon = (mastodon.isNull() || mastodon.isEmpty())? null: JsonUtil.toT(mastodon, APIEntity.class);
    } else {
      this.mastodon = null;
    }

  }

  private void checkConfig() throws RuntimeException {
    if (this.storage == null || this.proxy == null || this.download == null) {
      throw new NullPointerException("Configuration content not found.");
    }
    if (!checkStorageType(this.storage.getType())) {
      throw new IllegalArgumentException("Error storage type.");
    }
  }

  private boolean checkStorageType(String type) {
    return switch (type) {
      case "file", "s3" -> true;
      default -> false;
    };
  }
}
