/*
 * Copyright 2023 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.seppiko.canary;

import io.undertow.servlet.Servlets;
import io.undertow.servlet.api.FilterInfo;
import io.undertow.servlet.api.ServletInfo;
import java.net.InetSocketAddress;
import java.util.ArrayList;

import org.seppiko.canary.actions.*;
import org.seppiko.canary.filters.*;
import org.seppiko.canary.configures.CanaryConfiguration;
import org.seppiko.canary.utils.CanaryUtil;
import org.seppiko.canary.utils.UndertowUtil;
import org.seppiko.commons.logging.Logging;
import org.seppiko.commons.logging.LoggingFactory;

/**
 * @author Leonard Woo
 */
public class CanaryApplication {

  private static final Logging logger = LoggingFactory.getLogging(CanaryApplication.class);

  public static void main(String[] args) {
    try {
      CanaryUtil.printBanner(System.out);
      logger.info(CanaryUtil.getCurrentEnv());

      CanaryConfiguration config = CanaryConfiguration.getInstance();
      if (config.getTwitter() != null) {
        if(config.getTwitter().getToken().isBlank()) {
          logger.warn("MUST BE SET TWITTER API TOKEN");
        }
      }
      if (config.getMastodon() != null) {
        if(config.getMastodon().getToken().isBlank()) {
          logger.warn("MUST BE SET MASTODON API TOKEN");
        }
      }
      logger.info("User-Agent: " + CanaryConfiguration.UA);
      logger.info(CanaryUtil.getCurrentBuildTimestamp());

//      ArrayList<FilterInfo> filters = new ArrayList<>();
//      filters.add(Servlets.filter("clazzName", Class<? extend AbstractFilters>.class));

      ArrayList<ServletInfo> servlets = new ArrayList<>();
      servlets.add(Servlets.servlet("index", IndexAction.class).addMapping("/"));
      servlets.add(Servlets.servlet("twitter2", Twitter2Action.class).addMapping("/twitter"));
      servlets.add(Servlets.servlet("mastodon", MastodonAction.class).addMapping("/mastodon"));
      servlets.add(Servlets.servlet("metrics", MetricsAction.class).addMapping("/metrics"));

      InetSocketAddress bind = new InetSocketAddress(config.getPort());
      UndertowUtil undertow = new UndertowUtil();
      undertow.setRootPath("/");
      undertow.setExceptionHandler(new ErrorAction().get());
      undertow.start(bind, servlets);

      logger.info(String.format("Started server on http://%s:%d/ ", config.getHost(), config.getPort()));
    } catch (Throwable t) {
      logger.atError()
          .message("SERVICE ERROR")
          .withCause(t)
          .log();
    }
  }

}
