/*
 * Copyright 2023 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.seppiko.canary.filters;

import jakarta.servlet.*;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import java.util.Objects;
import org.seppiko.canary.models.ResponseEntity;

import java.io.IOException;
import java.nio.charset.StandardCharsets;

/**
 * Http servlet abstract filter
 *
 * @author Leonard Woo
 */
public abstract class AbstractFilters implements Filter {

  protected abstract ResponseEntity filterHandleExecution(HttpServletRequest request)
          throws IOException, ServletException;

  /** {@inheritDoc} */
  @Override
  public void init(FilterConfig filterConfig) throws ServletException {
  }

  /** {@inheritDoc} */
  @Override
  public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse, FilterChain filterChain)
          throws IOException, ServletException {
    HttpServletRequest req = (HttpServletRequest) servletRequest;
    HttpServletResponse resp = (HttpServletResponse) servletResponse;

    req.setCharacterEncoding(StandardCharsets.UTF_8.name());

    ResponseEntity fhxResp = filterHandleExecution(req);
    if (Objects.nonNull(fhxResp)) {
      resp.setCharacterEncoding(StandardCharsets.UTF_8.name());
      resp.setContentType(resp.getContentType());
    }

    filterChain.doFilter(req, resp);
  }

  /** {@inheritDoc} */
  @Override
  public void destroy() {
  }

}
